/***
*Author: Ke Yi Ren
*Student Number: s3405167
*version 2.6
*Date 15/4/14
*
*
*/




#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>


#define WIDTH 1200
#define HEIGHT 860
#define pi (4.0f*atanf(1.0f))



/*This is example is use of debug*/
#define DEBUG
#undef DEBUG

#if DEBUG
#endif

/*Mass*/
const float mass = 9.8;
//Setting up globar variable for light setting
GLfloat redDiffuseMaterial[] = {1.0, 0.0, 0.0};
GLfloat whiteSpecularMaterial[] = {1.0, 1.0, 1.0};
GLfloat greenEmissiveMaterial[] = {0.0, 1.0, 0.0};
GLfloat whiteSpecularLight[] = {1.0, 1.0, 1.0};
GLfloat blackAmbientLight[] = {0.0, 0.0, 0.0};
GLfloat whiteDiffuseLight[] = {1.0, 1.0, 1.0}; 

/*set up bool variable for enable/disband with true and false*/
typedef enum{ false, true} bool;

/*struct for toggle hotkey*/
typedef struct{
	bool drawNormal;
	bool drawBinormal;
	bool drawTangent;
	bool enableLight;
	bool enableWireframe;
	bool enableAxis;
} trigger;

trigger t = {false, false, false, false, true, true};

/*struct of delta timer, rotate for camera*/
typedef struct{
	float deltaAngle;
	float x_rotateAngle;
	float y_rotateAngle;
	float z_rotateAngle;
	float deltaZoom;
	float zoom;
	
} camera;

camera c = {0, 0, 0, 0, 0, -5};
camera playerFacing = {0, 0, 0, 0, 0, 0};

/*For MouseClick Function*/
//int drawFlag = 1, drawNormal = 1;
int activeZoom =0;
int activeRotation = 0;


typedef struct {
	float x, y, z;
} vertex_t;


vertex_t startPlayer = {0,0,0};
vertex_t movingPlayer = {0,0,0};
vertex_t delta = {0,0,0};
vertex_t player3rdCamera = {0,0,0};
vertex_t *vertices;
vertex_t facingAndPrepare = {0,0,0};
vertex_t lastUpdatePlayer = {0,0,0};
vertex_t force = {0,0,0};
/*set up date value for sine complex*/

typedef struct{
 	float lengthX;
	float lengthZ; 
	float curve; 
	float amplitude;
}sine;

sine s ={20, 10, 0.3, 1};

/*  Storing Vertex Coordinates Function to improve performation and effective coding . */
//Note: this is attempt try however run out of time... so might do after submit assignment 1.
void init_grid()
{
	vertex_t *grid;
}



/*Function to convert Degree to Radian*/
float convertToRadian(float *angle){
	float converted = *angle / 180 * pi;
	return converted;
}

/*Function to convert Degree to Degree*/
float convertToDegree(float radian){

	float converted = radian / M_PI * 180;
	return converted;
}

/*Setting up color*/
float colorBlue[] = { 0.0, 0.0, 1.0, 1.0 };
float colorWhite[] = { 1.0, 1.0, 1.0, 1.0 };
float colorRed[] = {1.0, 0.0, 0.0, 1.0};
float colorGreen[] = {0.0, 1.0, 0.0, 1.0};
float colorYellow[] ={1.0, 1.0, 0.0, 1.0};
float colorPink[] = {1.0, 0.0, 1.0, 1.0};
float colorTeal[] = {0.0, 1.0, 1.0, 1.0};
float colorOrange[] = {1.0, 0.7, 0, 1,0};

/*Struct of MousePoint Position*/
typedef struct{
int x;
int y;
}mouse_point;
mouse_point mousePoint= {0,0};

/* Declare variable for Animation of sinewaves and delta*/
vertex_t p = {0, -0.1, 0};
vertex_t d = {0, -0.1, 0};

/*Declare variable for updating player position*/

vertex_t playerPosition = {0,0,0};



//Function to calucation Normal
void calucationNormal(vertex_t *normal, vertex_t *vertex, float lengthX, float lengthZ, float amplitude)
{
	float kX = (2 * pi) / (float)lengthX, kZ = (2 * pi) / (float)lengthZ;
	//vertex_t vn = *a;
	
	normal->x = -amplitude * kX * cosf(kX * vertex->x + kZ * vertex->z);
	//normal->x = 1;
	normal->y = 1;
	//normal->z =0;
	normal->z = -amplitude * kZ * cosf(kZ * vertex->z + kX * vertex->x);

	float l = sqrtf(normal->x * normal->x + normal->y * normal->y + normal->z * normal->z);
	normal->x /= l;
	normal->y /= l;
	normal->z /= l;
	
}

//Function to Calucation Tangent 
void calucationTangent(vertex_t *tangent, vertex_t *vertex, float lengthX, float lengthZ, float amplitude){
	
	float kX = (2 * pi) / (float)lengthX, kZ = (2 * pi) / (float)lengthZ;
	
	
	tangent->x = 1;
	tangent->y = amplitude * kX * cosf(kX * vertex->x + kZ * vertex->z);
	tangent->z = 0;
	
	float l = sqrtf(tangent->x * tangent->x + tangent->y * tangent->y + tangent->z * tangent->z);
	
	tangent->x /= l;
	tangent->y /= l;
	tangent->z /= l;
	
}

//Function to Calucation Binormal

void calucationBinormal(vertex_t *binormal, vertex_t *vertex, float lengthX, float lengthZ, float amplitude){
	
	float kX = (2 * pi) / (float)lengthX, kZ = (2 * pi) / (float)lengthZ;
	
	
	binormal->x = 0;
	binormal->y = amplitude * kZ * cosf(kX * vertex->x + kZ * vertex->z);
	binormal->z = 1;
	
	float l = sqrtf(binormal->x * binormal->x + binormal->y * binormal->y + binormal->z * binormal->z);
	
	binormal->x /= l;
	binormal->y /= l;
	binormal->z /= l;
}





/*structure of draw vector of Normal*/
void drawNormalVector(float x, float y, float z, float dx, float dy, float dz)
{
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glColor3f(1.0, 1.0, 0.0);
	glVertex3f(x,y,z);
	glVertex3f(x + dx, y + dy, z+dz);
	glEnd();
	glEnable(GL_LIGHTING);
	
}
/*structure of draw vector of Tangent*/
void drawTangentVector(float x, float y, float z, float dx, float dy, float dz)
{
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glColor3f(0.0, 1.0, 1.0);
	glVertex3f(x,y,z);
	glVertex3f(x + dx, y + dy, z+ dz);
	glEnd();
	glEnable(GL_LIGHTING);
}

/*structure of draw vector of Binormal*/
void drawBinormalVector(float x, float y, float z, float dx, float dy, float dz)
{
	glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	glColor3f(1.0, 0.0, 1.0);
	glVertex3f(x,y,z);
	glVertex3f(x + dx, y + dy, z + dz);
	glEnd();
	glEnable(GL_LIGHTING);
}




/*setting up rotation camera with mouseMoveFuncion.*/
void mouseMove(int x, int y) { 
	
	//printf("%d, %d\n", x, y);

	float y2 = y; 
	float half_height = y2 / 2;

	if(activeRotation ==1)
	{
		
		c.x_rotateAngle += (y - mousePoint.y);
		c.y_rotateAngle += (x - mousePoint.x);
		#if 0
		printf("c.zoom = %f, x = %f, angleofX = %f \n", c.zoom, x, 	c.x_rotateAngle);
		printf("c.zoom = %f, y = %f, angleofY = %f \n", c.zoom, y, 	c.y_rotateAngle);
		#endif
	}

	//printf("%d \n", activeZoom);
	if (activeZoom ==1)
	{	
	
		c.zoom += (y - mousePoint.y) * 0.1;
					
	}
	//printf("c.zoom = %f, dy = %f, dx = %f \n", c.zoom, y - mousePoint.y, x - mousePoint.x);

	mousePoint.y = y;
	mousePoint.x = x;
}

void passiveMouseMove(int x, int y)
{
	float y2 = y;
	float height = y2 / 2; 
	mousePoint.x = x;
	mousePoint.y = y;
	
	//printf("passive c.zoom = %f, y = %f, height = %f \n", c.zoom, y2, height);


}

/*Mouse Control with right/left button function*/
void mouseButton(int button, int state, int x, int y) {
	
//if you hold left mouse click to for rotation camera
	if(button == GLUT_LEFT_BUTTON)
	{
		if(state == GLUT_DOWN)
		{
			activeRotation = 1;
			//angle += ;
			//printf("angle = %f\n", angle);
			
		}
		if(state == GLUT_UP)
		{
			activeRotation = 0;
		}

	}
//if you hold left mouse click to for zoom in or zoom out
	if(button == GLUT_RIGHT_BUTTON){
		if(state == GLUT_DOWN)
		{
			
			//zoom += deltaZoom;
			activeZoom = 1;
		}
		if(state == GLUT_UP)
		{
			activeZoom = 0;
		}
	}
	
}


/*Keyboard control*/
void keyDown(unsigned char key, int x, int y)
{
	//Vector of force
	
	
	//float _playerFacing = playerFacing.y_rotateAngle;
	float kX = (2 * pi) / (float)s.lengthX, kZ = (2 * pi) / (float)s.lengthZ;
	//printf("%c, %f, %f ", key, x,y);
	//Press n for enable/disable debug to show Normal Vector 
	if (key == 'n')
	{
		if(t.drawNormal == false){
			t.drawNormal = true;
		}
		else
		{
			t.drawNormal = false;
		}
	}
	
	//Press b for enable/disable debug to show Binormal Vector
	if ( key == 'b')
	{
		if(t.drawBinormal == false)
		{
			t.drawBinormal = true;
		}
		else
		{
			t.drawBinormal = false;
		}
	}

	// Press t for enable/disable debug to show Tangent Vector
	if ( key =='t')
	{
		if(t.drawTangent == false)
		{
			t.drawTangent = true;
		}
		else
		{
			t.drawTangent = false;
		}
	}

	//Press p for on/off wireframe mode
	if ( key == 'p'){
		if(t.enableWireframe == true)
		{
			t.enableWireframe = false;
		}
		else
		{
			t.enableWireframe = true;
		}
	}

	//Press l for turn on/off light system
	
	if( key =='l'){
		//printf("%c", t.enableLight);
		if(t.enableLight == false)
		{
			t.enableLight = true;
		}
		else
		{
			t.enableLight = false;
		}
	}

	if(key == 'o'){
		if(t.enableAxis == true){
			t.enableAxis = false;
		}
		else
		{
			t.enableAxis = true;
		}
	}

	//Hold w for moving foward
	if( key =='w' ){

		movingPlayer.x += cosf(convertToRadian(&playerFacing.y_rotateAngle)) * delta.x * 0.4;
		movingPlayer.z -= sinf(convertToRadian(&playerFacing.y_rotateAngle)) * delta.x * 0.4;
		
		movingPlayer.y = s.amplitude * sinf((kX * movingPlayer.x) + (kZ * movingPlayer.z));
		//movingPlayer.y = movingPlayer.x;
		vertex_t normal1 = {0,0,0};
		calucationTangent(&normal1, &movingPlayer, s.lengthX, s.lengthZ, s.amplitude);
		//float radian = tangent1.y / tangent1.x ;
		//playerFacing.z_rotateAngle = - atan(radian) / pi * 180;
		//double z = (asin(movingPlayer.y/s.amplitude) - ( kZ * movingPlayer.z))/ kX;
		//playerFacing.z_rotateAngle = convertToDegree();
		/*
		vertex_t v0 = normal1;
		vertex_t v1 = {normal1.x, movingPlayer.y,0};
		float a = v0.x * v1.x;
		float b = v0.y * v1.y;
		float ab = a + b;
		float ma = sqrtf(v0.x * v0.x + v1.x * v1.x);
		float mb = sqrtf(v0.y * v0.y + v1.y * v1.y);
		float angle = ab / (ma * mb);
		*/
		
		//printf("angle = %f \n", angle);
		lastUpdatePlayer = movingPlayer;

	}
	
	//Hold s for moving backward
	if(key == 's'){
		movingPlayer.x += cosf(convertToRadian(&playerFacing.y_rotateAngle)) * delta.x*0.4;
		movingPlayer.z -= sinf(convertToRadian(&playerFacing.y_rotateAngle)) * delta.x*0.4;

		//movingPlayer.y = movingPlayer.x;
		movingPlayer.y = s.amplitude * sinf((kX * movingPlayer.x) + (kZ * movingPlayer.z));
		vertex_t tangent2 = {0,0,0};
		//calucationNormal(&tangent2, &movingPlayer, s.lengthX, s.lengthZ, s.amplitude);
		//float radian2 =  tangent2.x / tangent2.y;
		//playerFacing.z_rotateAngle = atan(radian2);
		
	}
	

	//Hold a for a to moving to left
	if(key == 'a'){
		playerFacing.y_rotateAngle += c.deltaAngle * 0.1;
		if(playerFacing.y_rotateAngle > 360) playerFacing.y_rotateAngle -= 360;

	}
	
	//Hold d to moving to left
	if(key == 'd'){
		playerFacing.y_rotateAngle -= c.deltaAngle * 0.1;
		if(playerFacing.y_rotateAngle < -360) playerFacing.y_rotateAngle += 360;
		#if 0
		movingPlayer.z -= sinf(convertToRadian(&playerFacing.y_rotateAngle)) * delta.x *0.3;
		movingPlayer.x += cosf(convertToRadian(&playerFacing.y_rotateAngle)) * delta.x *0.3;
		vertex_t tangent4 = {0,0,0};
		calucationNormal(&tangent4, &movingPlayer, s.lengthX, s.lengthZ, s.amplitude);
		float radian4 =  tangent4.x / tangent4.y;
		playerFacing.z_rotateAngle = atan(radian4)/ pi * 180;
		#endif
	}

	// Exit Program
	if(key == 'q' || key == 27){
		exit(EXIT_SUCCESS);
	}
	//playerFacing.y_rotateAngle = _playerFacing;

}


/*  create 3D Axes  */
void createAxes(float x, float y, float z, float length)
{	glDisable(GL_LIGHTING);
	//glPushMatrix();
	//glDisable(GL_LIGHTING);
	glBegin(GL_LINES);
	//red line and x axes
	//glColor3f(1.0, 0.6, 0.7);
	glColor3f(1.0, 0.0, 0.0);	
	//glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, colorBlue);
	glVertex3f(x, y, z);
	glVertex3f(x + length, y, z);

	//green line and y axes
	glColor3f(0.0, 1.0, 0.0);
	glVertex3f(x,y, z );
	glVertex3f(x, y + length, z);

	//blue line and z axes	
	glColor3f(0.0, 0.0, 1.0);
	glVertex3f(x, y, z);
	glVertex3f(x , y, z + length);
	glEnd();
	glEnable(GL_LIGHTING);
	//glPopMatrix();
}	


/*Create 3D cube*/ 
void createCube(float x, float y, float z, float length)
{	
	float kX = (2 * pi) / (float)s.lengthX, kZ = (2 * pi) / (float)s.lengthZ;
	vertex_t cube = {x,y,z};
	
	glPushMatrix();

	

	
	glPushMatrix();
	vertex_t axeOfcube = {0, 0, 0};

	cube.x = -(length)/2;
	cube.y = -(length)/2;
	cube.z = -(length)/2;

	/*
	axeOfcube.x = (x+length)/2;
	axeOfcube.y = (y+length)/2;
	axeOfcube.z = (z+length)/2;
	*/
	
	createAxes(axeOfcube.x, axeOfcube.y, axeOfcube.z, length);
	//axeOfcube.x += movingPlayer.x;
	
	//Testing
	//cube.x = p.x;
	//

	
	

	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, colorOrange);
	//glColor3f(1.0, 0.7, 0);
	glBegin(GL_QUADS);
	
	//Buttom surface and 1st surface and facing to y
	glNormal3f(0, 1, 0);
	//glColor3f(1.0, 1.0, 1.0);
	glVertex3f(cube.x, cube.y, cube.z); //v0
	glVertex3f(cube.x+length, cube.y, cube.z); //v1
	glVertex3f(cube.x+length, cube.y, cube.z+length); //v2
	glVertex3f(cube.x, cube.y, cube.z+length); //v3
	
	//East surface and 2nd surface and facing to x
	glNormal3f(1, 0, 0);
	glVertex3f(cube.x+length, cube.y, cube.z); //v4
	glVertex3f(cube.x+length, cube.y, cube.z+length); //v5
	glVertex3f(cube.x+length, cube.y+length, cube.z+length); //v6
	glVertex3f(cube.x+length, cube.y+length, cube.z);	//7	
	
	//South surface and 3rd surface and facing to z
	glNormal3f(0, 0, 1);
	glVertex3f(cube.x, cube.y, cube.z); //v8
	glVertex3f(cube.x+length, cube.y, cube.z); //v9
	glVertex3f(cube.x+length, cube.y+length, cube.z); //v10
	glVertex3f(cube.x, cube.y+length, cube.z); //v11

	// West surface and 4th surface and facing to x
	glNormal3f(1, 0, 0);
	glVertex3f(cube.x, cube.y, cube.z); //v12
	glVertex3f(cube.x, cube.y, cube.z+length); //v13
	glVertex3f(cube.x, cube.y+length, cube.z+length); //v14
	glVertex3f(cube.x, cube.y+length, cube.z); //15

	// North Surface and 5th surface and facing to z
	glNormal3f(0, 0, 1);
	glVertex3f(cube.x+length, cube.y, cube.z+length);  //16
	glVertex3f(cube.x, cube.y, cube.z+length); //17
	glVertex3f(cube.x, cube.y+length, cube.z+length); //18
	glVertex3f(cube.x+length, cube.y+length, cube.z+length); //19
	
	//Top Surface and 6th surface and facing to y
	glNormal3f(0, 1, 0);
	glVertex3f(cube.x, cube.y+length, cube.z); //v20
	glVertex3f(cube.x+length, cube.y+length, cube.z); //v21
	glVertex3f(cube.x+length, cube.y+length, cube.z+length); //v22
	glVertex3f(cube.x, cube.y+length, cube.z+length); //v23
	
	glEnd();

	glPopMatrix();





	glPopMatrix();
}

/* FAMOUS TEAPOT IMPORT!!!! HAIL TO THE TEAPOT!*/
void createTeaPot(float size)
{
	vertex_t axesOnTeaPot = {0,0,0};
	createAxes(axesOnTeaPot.x,axesOnTeaPot.y,axesOnTeaPot.z, 1);
	
	glutWireTeapot(size);
	
}

/*draw a point which is animation for sinewave*/
void createPointOfSinewave()
{
	
	int length = 2, a = 1;
	float k = (2 * pi) / (float)length;
	glColor3f(1.0, 1.0, 1.0);
	glPointSize(10);
	glBegin(GL_POINTS);
	
	p.y = a * sinf(k * p.x);
	#if 0
	printf("pX = %f, pY = %f", pX, pY);
	#endif
	glVertex2f(p.x, p.y);
	glEnd();
}


/*draw basic flat grid*/
void createGrid(float x,float z, float curve)
{
	//float colorBlue[4] = { 0.0, 0.2, 1.0, 1.0 };
	vertex_t v0 = {x, 0, z};
	vertex_t v1 = {x, 0, z};
	vertex_t v2 = {x, 0, z};
	vertex_t v3 = {x, 0, z};
	//drawVector(x,y,z);
	//glEnable(GL_NORMALIZE);
	
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//glColor3f(1.0, 1.0, 1.0);

	//glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, colorBlue);
	
	glBegin(GL_TRIANGLE_STRIP);
	//glNormal3f(x, y+curve, z);
	glVertex3f(v0.x, v0.y, v0.z);
	glVertex3f(v1.x+curve, v1.y, v1.z);
	glVertex3f(v2.x, v2.y, v2.z+curve);
	glVertex3f(v3.x+curve, v3.y, v3.z+curve);
	glEnd();
	
}

#if 0

/*draw Normal, Tangent and Binormal Vector line for complex sine wave*/
void drawAllVectorLine(vertex_t read, float amplitude, float kx, float kz)
{

	//Calucation Normal Vector
	vertex_t normal = { 0, 0, 0};
	normal.x = -amplitude * kx * cosf(kx * read.x + kz * read.z);
	normal.y = 1;
	normal.z = -amplitude * kz * cosf(kx * read.x  + kz * read.z);
	
	float nl = sqrtf(normal.x * normal.x + normal.y * normal.y + normal.z * normal.z);
	normal.x /= nl;
	normal.y /= nl;
	normal.z /= nl;
	if(t.drawNormal == true){
		drawNormalVector(read.x, read.y, read.z, normal.x, normal.y, normal.z);
	}
	
	//Calucation Tangent Vector	
	vertex_t tangent = {0,0,0};
	
	tangent.x = 1;
	tangent.y = amplitude * kx * cosf(kx * read.x + kz * read.z);
	tangent.z = 0;
	
	float tl = sqrtf(tangent.x * tangent.x + tangent.y * tangent.y + tangent.z * tangent.z);
	
	tangent.x /= tl;
	tangent.y /= tl;
	tangent.z /= tl;
	if(t.drawTangent == true){
		drawTangentVector(read.x, read.y, read.z, tangent.x, tangent.y, tangent.z);
	}
	


	//Calucation Binormal vector

	vertex_t binormal = {0,0,0};
	
	binormal.x = 0;
	binormal.y = amplitude * kz * cosf(kx * read.x + kz * read.z);
	binormal.z = 1;
	
	float l = sqrtf(binormal.x * binormal.x + binormal.y * binormal.y + binormal.z * binormal.z);
	
	binormal.x /= l;
	binormal.y /= l;
	binormal.z /= l;
	
	
}
#endif

/* drawN Normal Vector line speficity for complex sine wave  it will fill every vertex*/
void drawNormalEachForSineWave(float x, float y, float z, float amplitude, float kx, float kz)
{
	
	vertex_t normal = { 0, 0, 0};
	normal.x = -amplitude * kx * cosf(kx * x + kz * z);
	normal.y = 1;
	normal.z = -amplitude * kz * cosf(kx * x + kz * z);
	
	float l = sqrtf(normal.x * normal.x + normal.y * normal.y + normal.z * normal.z);
	normal.x /= l;
	normal.y /= l;
	normal.z /= l;
	
	drawNormalVector(x, y, z, normal.x, normal.y, normal.z);
	
	//printf("%f = draw X's value\n", normal.x);
}

/*draw Tangent Vector line specifity for complex sine wave Note: it will fill every vertex*/
void drawTangentEachForSineWave(float x, float y, float z, float amplitude, float kx, float kz)
{
	vertex_t tangent = {0,0,0};
	
	tangent.x = 1;
	tangent.y = amplitude * kx * cosf(kx * x + kz * z);
	tangent.z = 0;
	
	float l = sqrtf(tangent.x * tangent.x + tangent.y * tangent.y + tangent.z * tangent.z);
	
	tangent.x /= l;
	tangent.y /= l;
	tangent.z /= l;
	
	drawTangentVector(x, y, z, tangent.x, tangent.y, tangent.z);
	
}


/*draw Binormal Vector line specifity for complex sine wavw  it will fill every vertex*/
void drawBinormalEachForSineWave(float x, float y, float z, float amplitude, float kx, float kz)
{
	vertex_t binormal = {0,0,0};
	
	binormal.x = 0;
	binormal.y = amplitude * kz * cosf(kx * x + kz * z);
	binormal.z = 1;
	
	float l = sqrtf(binormal.x * binormal.x + binormal.y * binormal.y + binormal.z * binormal.z);
	
	binormal.x /= l;
	binormal.y /= l;
	binormal.z /= l;
	
	drawBinormalVector(x, y, z, binormal.x, binormal.y, binormal.z);
	
}




/*Draw complex sinewave with x value and z value*/
void createGridwithSineWave(float x, float z, float lengthX, float lengthZ, float curve, float amplitude)
{	

	/*Delcare variable for next curve and its purpose for apply on */
	float xCurve = x + curve;
	float zCurve = z + curve;
	float kX = (2.0f * pi) / (float)lengthX, kZ = (2.0f * pi) / (float)lengthZ; 

	vertex_t v0 = {x, 0, z};

	
	v0.y = amplitude * sinf((kX * v0.x) + (kZ * v0.z));
	//v0.y = v0.x; //+ v0.z;
	vertex_t v0n = {0,0,0};
	calucationNormal(&v0n, &v0, lengthX, lengthZ, amplitude);


	vertex_t v1 = {x, 0, zCurve};
	vertex_t v1n = {0,0,0};
	
	v1.y = amplitude * sinf((kX * v1.x) + (kZ * v1.z));
	//v1.y = v1.x; //+ v1.z;
	calucationNormal(&v1n, &v1, lengthX, lengthZ, amplitude);

	vertex_t v2 = {xCurve, 0, z};
	vertex_t v2n = {0,0,0};
	
	v2.y = amplitude * sinf((kX * v2.x) + (kZ * v2.z));
	//v2.y = v2.x; // + v2.z;
	calucationNormal(&v2n, &v2, lengthX, lengthZ, amplitude);

	vertex_t v3 = {xCurve, 0 , zCurve};
	//v3.y = v3.x; //+ v3.z;
	vertex_t v3n = {0,0,0};

	v3.y = amplitude * sinf((kX * v3.x) + (kZ * v3.z));
	calucationNormal(&v3n, &v3, lengthX, lengthZ, amplitude);

	
	//double y = a * sinf(k * x);
	

	
	//glColor3f(1.0, 1.0, 1.0);

	//Draw complex sinewave grid with Triangle Strip mode and added Normal on each vertex from Calucation function


	glBegin(GL_TRIANGLE_STRIP);
	
	
	//y0 = amplitude * sinf((kX * x) + (kZ * z));
	//y = amplitude * sinf((k * x));
	
	//yd = amplitude * sinf(k * xd);
	//yd = amplitude * sinf(k * xd);
	
	
	glNormal3f(v0n.x, v0n.y, v0n.z);
	glVertex3f(v0.x, v0.y, v0.z); //v0

	glNormal3f(v1n.x, v1n.y, v1n.z);	
	glVertex3f(v1.x, v1.y, v1.z);//v1
	
	glNormal3f(v2n.x, v2n.y, v2n.z);
	glVertex3f(v2.x, v2.y, v2.z);//v2
	
	glNormal3f(v3n.x, v3n.y, v3n.z);
	glVertex3f(v3.x, v3.y, v3.z);//v3



	glEnd();


	//Hotkey for enable/disable to debugging normal, Tagent and Bionormal

	if(t.drawNormal == true){
		drawNormalEachForSineWave(v0.x, v0.y, v0.z, amplitude, kX, kZ);
		drawNormalEachForSineWave(v1.x, v1.y, v1.z, amplitude, kX, kZ);
		drawNormalEachForSineWave(v2.x, v2.y, v2.z, amplitude, kX, kZ);
		drawNormalEachForSineWave(v3.x, v3.y, v3.z, amplitude, kX, kZ);
	}

	if(t.drawTangent == true){
		drawTangentEachForSineWave(v0.x, v0.y, v0.z, amplitude, kX, kZ);
		drawTangentEachForSineWave(v1.x, v1.y, v1.z, amplitude, kX, kZ);
		drawTangentEachForSineWave(v2.x, v2.y, v2.z, amplitude, kX, kZ);
		drawTangentEachForSineWave(v3.x, v3.y, v3.z, amplitude, kX, kZ);
	}
	
	if(t.drawBinormal == true){
		drawBinormalEachForSineWave(v0.x, v0.y, v0.z, amplitude, kX, kZ);
		drawBinormalEachForSineWave(v1.x, v1.y, v1.z, amplitude, kX, kZ);
		drawBinormalEachForSineWave(v2.x, v2.y, v2.z, amplitude, kX, kZ);
		drawBinormalEachForSineWave(v3.x, v3.y, v3.z, amplitude, kX, kZ);
	}

}



/*Trying code vertex array*/

#if 0
int nv;
vertex_t *vertices;


void init_sine(int x_size, int y_size)
{

	double kX = (2 * pi) / (double)lengthX, kZ = (2 * pi) / (double)lengthZ;
	
	vertex_t *vertex;

	/*Allocate array*/
	nv = (x_size+1)*(y_size+1);
	vertices = calloc(nv, sizeof(vertex_t));
	if(!vertices)
	exit(1);
	
	int j = 0;
	int i = 0;


}


void init_grid(int x_size, int y_size)
{
	vertex_t *vertex;
	/*Allocate array*/
	nv = ( (x_size + 1) * (y_size + 1) );

	vertices = calloc(nv, sizeof(vertex_t));
	
	int j =0, i = 0;
	
	for (j =0; j <=nv; j++){
		for(i = 0; i <=nv; i++){
			vertex->x = i;
			vertex->y = 0;
			vertex->z = j;
			vertex++;
		}
	}	
	
}


void draw_grid()
{
	int i;
	glPointSize(2.0f);
	for(i = 0; i < 3; i++)
		glVertex3fv((float *) &vertices[i]);
	glEnd();
}
#endif


/*Draw circle for testing/fun and research*/

void drawCircle()
{

	const int slices=2, stacks=4;
	float theta, phi;
	float x1, y1, z1, x2, y2, z2, x3, y3, z3, x4, y4, z4;
	float step_theta = 2.0 * M_PI / slices, step_phi = M_PI / stacks;
	float r = 1;
	int j =0;
	int i = 0;

	glBegin(GL_TRIANGLES);
	for (j = 0; j <= stacks; j++) {
	  phi = j / (float)stacks * M_PI;
	  for (i= 0; i < slices; i++) {
	    theta = i / (float)slices * 2.0 * M_PI;
	    x1 = r * sinf(phi) * cosf(theta);
	    y1 = r * sinf(phi) * sinf(theta);
	    z1 = r * cosf(phi);
	    x2 = r * sinf(phi) * cosf(theta + step_theta);
	    y2 = r * sinf(phi) * sinf(theta + step_theta);
	    z2 = r * cosf(phi);
	    x3 = r * sinf(phi + step_phi) * cosf(theta + step_theta);
	    y3 = r * sinf(phi + step_phi) * sinf(theta + step_theta);
	    z3 = r * cosf(phi + step_phi);
	    x4 = r * sinf(phi + step_phi) * cosf(theta);
	    y4 = r * sinf(phi + step_phi) * sinf(theta);
	    z4 = r * cosf(phi + step_phi);
	    glVertex3f(x1, y1, z1);
	    glVertex3f(x2, y2, z2);
	    glVertex3f(x3, y3, z3);
	    glVertex3f(x1, y1, z1);
	    glVertex3f(x3, y3, z3);
	    glVertex3f(x4, y4, z4);
	  }
	}
	glEnd();

}


/*Making SnowGround with width and height and using complex sine grid function.*/

void CreateSnowGround(float xSize, float zSize)
{
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, colorWhite);
	/*Delcare variable for loop with width and height*/
	float x = 0, z = 0;
	
	
	for(x = -xSize; x <= xSize; x += s.curve)
	{
		for (z = zSize; z >= -zSize; z -= s.curve)
		{	//createGrid(x,z,c);
			//printf("x = %f y = %f \n", a, b);
			createGridwithSineWave(x,z, s.lengthX, s.lengthZ ,s.curve, s.amplitude);
			//printf("x = %f, z = %f\n", xSize, zSize);
		}
	}


}


vertex_t player = {0,0,0};

/*Setting up Lighting*/
void init(void) 
{


		
	
	   GLfloat mat_specular[] = { 1.0, 1.0, 1.0, 1.0 };
	   GLfloat mat_shininess[] = { 2.0 };
	   
	   glClearColor (0.0, 0.0, 0.0, 0.0);
	   glShadeModel (GL_SMOOTH);
	
	   glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	   glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
	   
	
	//glDisable(GL_LIGHTING);
	/*
	glLightfv(GL_LIGHT0, GL_SPECULAR, whiteSpecularLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT, blackAmbientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, whiteDiffuseLight);
	*/
	GLfloat light_position[] = { 1.0, 1.0, 1.0, 0.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, light_position);
	//glEnable(GL_NORMALIZE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_DEPTH_TEST);
	glShadeModel(GL_SMOOTH);
	
	if (t.enableLight == false){
		glDisable(GL_LIGHTING);
	}
	
}

/*Renderer display function*/
void display()
{	
	
	

	float a = 0, b = 0;
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glLoadIdentity();

	if(t.enableWireframe == true){
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	// initization value of 3rd person Camera
	player3rdCamera.x = 1 - movingPlayer.x;
	player3rdCamera.z = 1 - movingPlayer.z;
	player3rdCamera.y = 0.01;

	/*Camera Translate*/
	glTranslatef( 0, 0, c.zoom);
	//glScalef(zoom, zoom, zoom);
	/*Set up 3rd person Camera*/
	glRotatef(c.x_rotateAngle, 1, 0, 0);
	glRotatef(c.y_rotateAngle, 0, 1, 0);
	glRotatef(c.z_rotateAngle, 0, 0, 1);
	glTranslatef(player3rdCamera.x, player3rdCamera.y, player3rdCamera.z );
	
	//printf("angle = %f\n", angle);
	//glRotatef(c_x, 0, 0, 1);
	//Set the camera
	vertex_t my_axeWorld = {0,0,0};
	if(t.enableAxis == true){
		createAxes(my_axeWorld.x, my_axeWorld.y, my_axeWorld.z, 100);
	}
	init();
	CreateSnowGround(10, 10);
	glPushMatrix();

	
	//player.x = movingPlayer.x;
	//glTranslatef(player.x, player.y, player.z);
	glTranslatef(movingPlayer.x, movingPlayer.y, movingPlayer.z);
	glPushMatrix();
	glRotatef(playerFacing.y_rotateAngle, 0, 1, 0); // yaw	
	glRotatef(playerFacing.z_rotateAngle, 0, 0, 1); // pitch
	glRotatef(playerFacing.x_rotateAngle, 1, 0, 0); // roll	
	//printf("pitch = %f \n" , playerFacing.z_rotateAngle);
	createCube(startPlayer.x, startPlayer.y, startPlayer.z, 0.1);
	glPopMatrix();

	glutSwapBuffers();
}








/* reshape the window function  */

void changeSize(int w, int h){

	// Set the viewport to be the entire window
	glViewport(0, 0, w, h);

	// Prevent a divided by zero, when window is too short
	// you can't make a window with zero width value.
	if( h == 0)
		h = 1;
	float ratio = 1.0 * w/(float)h;

	// Use the projection Matrix
	glMatrixMode(GL_PROJECTION);
	
	// Reset Matrix
	glLoadIdentity();

	// Set the correct perspective.
	gluPerspective(60,ratio,0.01,100); // That's the field of view and is it can be modifty for zoom. Technically it's not a zoom.
	//glOrtho(-1, 1, -1, 1, -1, 1);
	// Get Back to the Modelview
	glMatrixMode(GL_MODELVIEW);
}

void idle()
{
	float t, dt;
	static float tLast = 0.0;
	float v = 0.5;

	/*Get elapsed time and covert to s */
	t = glutGet(GLUT_ELAPSED_TIME);
	t /= 1000.0;
	
	/* Calculate delta t */
	dt = t - tLast;
	
	/*Update velocity and position*/
	
	p.x += v * dt *10;
	d.x += v * dt;
	c.deltaAngle += v * dt * 5;
	c.deltaZoom += v * dt;
	delta.x = v * dt * 10;


	if (p.x > 10.0)
		p.x = -1.0;

	if (d.x > 1.0)
		d.x = -1.0;


	
	/* Update tLast for next time. using static local variable*/

	tLast = t;

	/*Ask glut to schedule call to display function	*/
	glutPostRedisplay();
}


int main(int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(320,320);
	glutCreateWindow("Assignment1");
	init();
	
	/* In this program these OpenGL calls only need to be done once,
           so they are in main rather than display. */
	//
	//glEnable(GL_DEPTH_TEST);
	
	
	
	

	glutDisplayFunc(display);
	glutMouseFunc(mouseButton);
	glutMotionFunc(mouseMove);
	glutPassiveMotionFunc(passiveMouseMove);
	glutKeyboardFunc(keyDown);

	// Here is our new entry in the main function
	glutReshapeFunc(changeSize);
	glutIdleFunc(idle);
	
	glutMainLoop();
}







