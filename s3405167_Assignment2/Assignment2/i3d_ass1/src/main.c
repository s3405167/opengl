
#include "gl.h"
#include "globals.h"
#include "stdio.h"
#include "math.h"
#include "tree.h"
#include "skybox.h"
#define Width 1024
#define UNUSED(x) (void)(x)





int c = 0; //count Number for make trees
float time = 0;




vec3f tree[100];
//a = {0,0,0};


Globals globals, globals2;
//GLuint texture;
static GLuint textureTree;
static GLuint textureSnow;
static GLuint textureSnowball;
static GLuint textureSkybox[6];


/*
struct Snowball{
	size_t array;
	Particle *particles;
};
*/
Particle snowball[100];




const int w = 1546;
//Distance
vec3f distance = {0,0,0};

static float getDeltaTime()
{
	// Returns time since previous frame in seconds.
	static int t1 = -1;

	if (t1 == -1)
	t1 = glutGet(GLUT_ELAPSED_TIME);
	int t2 = glutGet(GLUT_ELAPSED_TIME);
	float dt = (t2 - t1) / 1000.0f;
	t1 = t2;
	return dt;
}







#if 0
void RenderAllSnowBall(Particle *snowball, Camera *camera){

}
#endif

/*
TreeArray *cTree(size_t numberOfTree)
{
	TreeArray *treeArray = (TreeArray*) malloc(sizeof(TreeArray));
	treeArray->numberOfTree = numberOfTree;

	treeArray->vertices = (Vertex*) calloc(treeArray->numberOfTree, sizeof(Vertex));
	return treeArray;
}
*/








#if 0
//Generator random 100 tree placement
TreeArray *makeT(){
	TreeArray *treeArray = cTree(100);
	
}
#endif

void RenderText()
{
}





//Function for point to point intersection for collision Dectection

void pointToPointIntersection(vec3f *player, vec3f *object){
	//Debug to know object and player

	
	if(player->x == object->x && player->z == object->x ){
		player->x = object->x;
		player->z = object->z;
		printf("COLLISION!!!!!!!!!!!");
	}
 
}


void SphereToSphereIntersection(vec3f player, vec3f object)
{
	#if 0
	printf("object x = %lf , y = %lf, z = %lf \n", object.x, object.y, object.z);
	printf("player x = %lf , y = %lf, z = %lf \n", player.x, player.y, player.z);
	#endif	

	distance = subVec3f(player, object);
	float d = sqrt(distance.x * distance.x + distance.y * distance.y + distance.z * distance.z);
	float r1 = 2.5;
	float r2 = 2.5;
	//printf("d = %lf \n", d);
	if (d < (r1 * r1 + r2 * r2)){
		printf("OVERLAP! \n");
	}
	else
	{
		//printf("DISJOINT \n");
	}

	
}



#if 0
//Debug test
void init100Tree(Camera *camera){
vec3f a = {0,2,0};
int i = 50;
//printf(" i = %d, x = %lf, y = %lf, z = %lf \n", i, globals.grid.mesh->vertices[i].p.x, globals.grid.mesh->vertices[i].p.y, globals.grid.mesh->vertices[i].p.z);
//printf("x = %lf, y = %lf, z = %lf \n", globals.player.pos.x, globals.player.pos.y, globals.player.pos.z);
/*
for(int i = 0; i < 1000; i+= 5){
	//vec3f a = globals.grid.mesh->vertices[i].p;
	makeTree(camera,a);
	printf(" i = %d, y = %lf \n", i, globals.grid.mesh->vertices[i].p.y);
}
*/
}

#endif

static void updateKeyChar(unsigned char key, bool state)
{
	// Sets flags inside player controls, currently using w/a/s/d and arrow left, arrow right, arrow up, arrow down
	//printf("key = %c \n", key);
	switch (key)
	{
	case 'w':
		globals.playerControls.up = state;
		//printf("Player1!\n");
		break;

	case 's':
		globals.playerControls.down = state;
		break;

	case 'a':
		globals.playerControls.left = state;
		break;

	case 'd':
		globals.playerControls.right = state;
		break;
	//case 'e':
	
	
		
	default:
		break;
	}
}


static void updateSpecialKeyChar(int key, bool state)
{
	switch (key)
	{
		case GLUT_KEY_UP:
			globals2.playerControls.up = state;
			//printf("Player2!\n");
			break;
		case GLUT_KEY_DOWN:
			globals2.playerControls.down = state;
			break;


		case GLUT_KEY_LEFT:
			globals2.playerControls.left = state;
			break;

		case GLUT_KEY_RIGHT:
			globals2.playerControls.right = state;
			break;
	
	default:
	//updateKeyChar(key, true);
		break;
		
	}

}


static void renderViewport(Globals *globals, Grid *grid, Camera *camera)
{
	
	#if 0
	for (int i = 0; i < 100; i++)
	{
		
		printf(" i = %d, x = %lf, y = %lf, z = %lf \n ", i, a[i].x, a[i].y, a[i].z);
		printf(" i = %d, x = %lf, y = %lf, z = %lf \n ", i, globals->grid.mesh->vertices[i].p.x, globals->grid.mesh->vertices[i].p.y, globals->grid.mesh->vertices[i].p.z);		
	}
	#endif
	
	// Renders a viewport using the given camera. Useful to have this in its own function in case you want to render multiple viewports using different cameras.
	// 
	uploadProjection(camera);
	uploadView(camera);
	
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureSnow);
	renderGrid(grid, &globals->debugControls);
	glDisable(GL_TEXTURE_2D);
	renderPlayer(&globals->player, globals->testCube, &globals->debugControls);
	renderLight(&globals->light);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	
	
	//renderSkyBox(textureSkybox, &globals->player, camera, 0.5f);
	

	int i;
	for (i = 0; i < 100; i++){
		MakeSnowball(snowball[i], camera, textureSnowball);
		makeTree(tree[i], camera, textureTree);
	}
	glDisable(GL_BLEND);
	//printf("player x = %lf, y = %lf, z = %lf \n", globals->player.pos.x, globals->player.pos.y, globals->player.pos.z);
	
	
	


}



static void render(void)
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderViewport(&globals, &globals.grid, &globals.camera); // Render scene from perspective of first player, using left viewport
	renderViewport(&globals2, &globals.grid, &globals2.camera); // Render scene again from perspective of second player, using right viewport
	

	glutSwapBuffers();

}

static void update(void)
{
	float dt = getDeltaTime();
	//printf("dt = %lf \n", dt);
	//printf("time = %lf \n", time);
	
	updatePlayer(&globals.player, dt, &globals.grid, &globals.playerControls);
	updatePlayer(&globals2.player, dt, &globals.grid, &globals2.playerControls);
	UpdatingSnowEffect(dt, snowball, &globals.player, time);
	int i;
	//pointToPointIntersection(&globals.player.pos, &a[0]);
	for(i = 0; i < 100; i++)
	SphereToSphereIntersection(globals.player.pos, tree[i]);
	
	glutPostRedisplay();
}

static void reshape(int width, int height)
{	

	// So now you have two cameras, you need to update their values here
	globals.camera.right = width/2;
	globals.camera.height = height;
	globals2.camera.left = width/2;
	globals2.camera.right = width;
	globals2.camera.height = height;
 	
	// So that the program will continue rendering as the window is resized.
	render(); //is that from Opengl library?
}

static void mouseMove(int x, int y)
{
	static int prevX, prevY;

	int dx = x - prevX;
	int dy = y - prevY;

	if (globals.cameraControls.rotating){
		updateRotation(&globals.camera, dy, dx);
		updateRotation(&globals2.camera, dy, dx);
	}

	if (globals.cameraControls.zooming){
		updateZoom(&globals.camera, dy);
		updateZoom(&globals2.camera, dy);
	}

	prevX = x;
	prevY = y;
}

static void mouseDown(int button, int state, int x, int y)
{
	UNUSED(x);
	UNUSED(y);

	// Sets flags inside camera controls, currently just uses the left and right mouse buttons.
	if (button == GLUT_LEFT_BUTTON){
		globals.cameraControls.rotating = state == GLUT_DOWN;
		globals2.cameraControls.rotating = state == GLUT_DOWN;
	}
	if (button == GLUT_RIGHT_BUTTON){
		globals.cameraControls.zooming = state == GLUT_DOWN;	
		globals2.cameraControls.zooming = state == GLUT_DOWN;
	}
}

static void keyboard(unsigned char key, int x, int y)
{
	UNUSED(x);
	UNUSED(y);
	printf("key = %c \n", key);
	// Toggles flags in debug controls (as well as quitting using escape).
	switch (key)
	{
	case 27:
	case 'q':
		exit(EXIT_SUCCESS);
		break;

	case 'p':
		globals.debugControls.wireframeFlag = !globals.debugControls.wireframeFlag;
		toggleWireframe(&globals.debugControls);
		toggleWireframe(&globals2.debugControls);
		break;

	case 'l':
		globals.debugControls.lightingFlag = !globals.debugControls.lightingFlag;
		toggleLighting(&globals.debugControls);
		toggleLighting(&globals2.debugControls);
		break;

	case 'n':
		globals.debugControls.normalFlag = !globals.debugControls.normalFlag;
		globals2.debugControls.normalFlag = !globals2.debugControls.normalFlag;
		break;

	case 't':
		globals.debugControls.tangentFlag = !globals.debugControls.tangentFlag;
		globals2.debugControls.tangentFlag = !globals2.debugControls.tangentFlag;
		break;

	case 'b':
		globals.debugControls.binormalFlag = !globals.debugControls.binormalFlag;
		globals2.debugControls.binormalFlag = !globals2.debugControls.binormalFlag;
		break;

	case 'o':
		globals.debugControls.axisFlag = !globals.debugControls.axisFlag;
		globals2.debugControls.axisFlag = !globals2.debugControls.axisFlag;
		break;

	// If any other key is pressed, check if it's used for player movement.
	default:
		updateKeyChar(key, true);
		//updateSpecialKeyChar(key, true);
		break;
	}
}

static void keyboardUp(unsigned char key, int x, int y)
{
	UNUSED(x);
	UNUSED(y);

	// Key has been released, check if it's used for player movement.
	updateKeyChar(key, false);
	//updateSpecialKeyChar(key, false);
}	

static void updateSpecialKeyCharReleased(int key, int x, int y)
{
	UNUSED(x);
	UNUSED(y);

	updateSpecialKeyChar(key, false);
}



int main(int argc, char **argv)
{	
	
	// Window is created using data from the camera, so create that first.
	initCamera(&globals.camera, 0, w/2);
	initCamera(&globals2.camera, w/2, w);
	CreateArrayOfSnowball(snowball, &globals.player);
	
	glutInit(&argc, argv);
	glutInitWindowSize(globals.camera.right, globals.camera.height);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutCreateWindow("I3D Assignment");
	
	// Various callback functions being used.
	textureTree = loadTexture("tree.png");
	textureSnow = loadTexture("snow.jpg");
	textureSnowball = loadTexture("snowball.png");
	textureSkybox[0] = loadTexture("posx.bmp");
	textureSkybox[1] = loadTexture("posx.bmp");
	textureSkybox[2] = loadTexture("posx.bmp");
	textureSkybox[3] = loadTexture("posx.bmp");
	textureSkybox[4] = loadTexture("posx.bmp");
	textureSkybox[5] = loadTexture("posx.bmp");
	glutDisplayFunc(render);
	glutIdleFunc(update);
	glutReshapeFunc(reshape);
	glutMotionFunc(mouseMove);
	glutPassiveMotionFunc(mouseMove);
	glutMouseFunc(mouseDown);
	//Added special key such like arrow up key etc
	glutSpecialFunc(updateSpecialKeyChar);
	glutSpecialUpFunc(updateSpecialKeyCharReleased);

	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardUp);
	
	// Globals need to be initialised before the program starts running.
	initGlobals(&globals);
	initGlobals(&globals2);
	getplacementTree(tree, &globals.grid);
	sortBillBoard(tree);
	#if 0
	int a;
	for(a = 0; a < 100; a++)
	printf("array = %d , tree.z = %lf \n ", a, tree[a].z);
	#endif
	glutMainLoop();

	return EXIT_SUCCESS;
}










#if 0

/*
 * blockOnRamp.C
 *
 * This program shows the use of numerical integration of the
 * equations of motion to simulate a block sliding on a ramp (inclined
 * plane) 
 *
 * $Id: blockOnRamp.C,v 1.1 2014/05/12 00:24:30 gl Exp gl $
 *
 */

#include <math.h>
#include <sys/time.h>
#include <stdio.h>
#include <string.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
typedef int bool;
enum {false, true};
bool debug = true;
bool go = false;

typedef struct { float x, y; } Vec2f;
typedef struct { 
  Vec2f r, v, a;	// position, velocity, acceleration
  float m;		// mass
  float cd;		// drag coefficient
} BlockState;
BlockState block = { 
  { 0.0, 0.0 },  
  { 0.0, 0.0 }, 
  { 0.0, 0.0 },
  100.0,
  1.0
};

const float gg = -9.8;
typedef struct { Vec2f v0, v1; float angle; } Ramp;
Ramp ramp = { {0.0, 0.0}, {0.0, 0.0}, 10.0 / 360.0 * 2.0 * 3.14 };

float startTime;
const int milli = 1000;

void init(void) 
{
  // Ramp endpoints
  ramp.v0.x = cosf(ramp.angle);
  ramp.v0.y = sinf(ramp.angle);
  ramp.v1.x = -cosf(ramp.angle);
  ramp.v1.y = -sinf(ramp.angle);

  // Set block at side of plane
  block.r = ramp.v0;
}

void updateBlockState(float dt)
{
  float F, W, Fdrag;

  // Calculate resultant force

  // Weight
  W = block.m * gg;	

  // Drag 
  Fdrag = block.cd * (block.v.x * block.v.x + block.v.y * block.v.y);

  // Friction - none

  // Sum forces
  F = W * sinf(ramp.angle) + Fdrag;
  if (debug)
    printf("updateBlockState: F W D %f %f %f\n", F, W, Fdrag);

  // Acceleration 
  block.a.x = F * cosf(ramp.angle) / block.m;
  block.a.y = F * sinf(ramp.angle) / block.m;

  if (debug)
    printf("updateBlockState: a %f %f\n", block.a.x, block.a.y);

  // Integrate

  // Position 
  block.r.x += block.v.x * dt;
  block.r.y += block.v.y * dt;

  // Velocity 
  block.v.x += block.a.x * dt;
  block.v.y += block.a.y * dt;
}

void displayRamp(void)
{
  glBegin(GL_LINES);
  glVertex2f(ramp.v0.x, ramp.v0.y);
  glVertex2f(ramp.v1.x, ramp.v1.y);
  glEnd();
}

void displayBlock(void)
{
  glPushMatrix();
  glTranslatef(block.r.x, block.r.y, 0.0); 
  glutWireCube(0.1);
  glPopMatrix();
}


// Idle callback for animation
void update(void)
{
  static float lastT = -1.0;
  float t, dt;

  if (!go) 
    return;

  t = glutGet(GLUT_ELAPSED_TIME) / (float)milli - startTime;

  if (lastT < 0.0) {
    lastT = t;
    return;
  }

  dt = t - lastT;
  if (debug)
    printf("%f %f\n", t, dt);
  updateBlockState(dt);
  lastT = t;

  glutPostRedisplay();
}

void display(void)
{
  GLenum err;

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glColor3f (0.8, 0.8, 0.8);

  // Display block and plane
  glPushMatrix();
  displayBlock();
  displayRamp();
  glPopMatrix();

  glutSwapBuffers();
  // Check for errors
  while ((err = glGetError()) != GL_NO_ERROR)
    printf("%s\n",gluErrorString(err));
}


void keyboardCB(unsigned char key, int x, int y)
{
  switch (key) {
  case 's':
    if (!go) {
      startTime = glutGet(GLUT_ELAPSED_TIME) / (float)milli;
      go = true;
    }
    break;
  }
  glutPostRedisplay();
}

void myReshape(int w, int h)
{
  glViewport(0, 0, w, h);
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();

  glOrtho(-1.0, 1.0, -1.0, 1.0, -1.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
}

/*  Main Loop
 *  Open window with initial window size, title bar, 
 *  RGBA display mode, and handle input events.
 */
int main(int argc, char** argv)
{
  glutInit(&argc, argv);
  glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
  glutInitWindowSize(400, 400);
  glutInitWindowPosition(500, 500);
  glutCreateWindow("Block on Inclined Plane");
  glutKeyboardFunc(keyboardCB);
  glutReshapeFunc(myReshape);
  glutDisplayFunc(display);
  glutIdleFunc(update);

  init();

  glutMainLoop();
}
#endif

