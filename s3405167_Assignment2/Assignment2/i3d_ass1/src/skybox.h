#pragma once

#if __cplusplus
extern "C" {
#endif

#include "gl.h"
#include "camera.h"
#include "player.h"

void renderSkyBox(GLuint *texture, Player *player, Camera *camera, float size);
#if __cplusplus
}
#endif
