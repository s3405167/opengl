#pragma once

#if __cplusplus
extern "C" {
#endif

#include "camera.h"
#include "mesh.h"
#include "controls.h"
#include "player.h"
#include "grid.h"
#include "light.h"
#include "particle.h"
typedef struct Globals Globals;

///
/// Everything that is stored by the game can be found here. Just makes things easier.
///
struct Globals
{
	Camera camera;
	PlayerControls playerControls;	
	CameraControls cameraControls;
	DebugControls debugControls;
	Player player;
	Player player2;
	Grid grid;
	Light light;
	Mesh *testCube;
};

///
/// Initialises variables in globals with default values, and puts OpenGL into a default state for rendering. Expects globals to already have been constructed.
///
void initGlobals(Globals *globals);

#if __cplusplus
}
#endif
