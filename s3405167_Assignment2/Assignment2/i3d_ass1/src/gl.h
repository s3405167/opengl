#pragma once

#if __cplusplus
extern "C" {
#endif

// This allows use OpenGL2+ functions, which you may or may not want to use for assignment 2
#include <GL/glew.h>

#ifdef __APPLE__
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#else
#  if _WIN32
#    include <Windows.h>
#  endif
#  include <GL/gl.h>
#  include <GL/glu.h>
#  include <GL/glut.h>
#endif

///
/// Loads image file given by filename into an OpenGL texture object (returns 0 on fail), wrapper around SOIL image library.
///
GLuint loadTexture(const char *filename);

///
///Load skybox image file given by filename into an OpenGL texture object (returns 0 on fail), wrapper around SOIL image library.
///
//GLuint loadSkyBoxTexture(const char *filename2)

#if __cplusplus
}
#endif
