#include "mesh.h"
#include "controls.h"
#include "gl.h"

#include <math.h>

static void renderAxes()
{
	glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);

	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glBegin(GL_LINES);

	glColor3f(1, 0, 0);
	glVertex3f(0, 0, 0); glVertex3f(5, 0, 0);

	glColor3f(0, 1, 0);
	glVertex3f(0, 0, 0); glVertex3f(0, 5, 0);

	glColor3f(0, 0, 1);
	glVertex3f(0, 0, 0); glVertex3f(0, 0, 5);

	glEnd();

	glPopAttrib();
}

static void renderLines(Mesh *mesh, bool normalFlag, bool tangentFlag, bool binormalFlag)
{
	glPushAttrib(GL_CURRENT_BIT | GL_ENABLE_BIT);

	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D);

	glBegin(GL_LINES);

	for (size_t i = 0; i < mesh->nIndices; i++)
	{
		int j = mesh->indices[i];

		if (normalFlag)
		{
			glColor3f(1, 1, 0);
			glVertex3f(mesh->vertices[j].p.x, mesh->vertices[j].p.y, mesh->vertices[j].p.z);
			glVertex3f(mesh->vertices[j].p.x + mesh->vertices[j].n.x, mesh->vertices[j].p.y + mesh->vertices[j].n.y, mesh->vertices[j].p.z + mesh->vertices[j].n.z);
		}
		if (tangentFlag)
		{
			glColor3f(0, 1, 1);
			glVertex3f(mesh->vertices[j].p.x, mesh->vertices[j].p.y, mesh->vertices[j].p.z);
			glVertex3f(mesh->vertices[j].p.x + mesh->vertices[j].tan.x, mesh->vertices[j].p.y + mesh->vertices[j].tan.y, mesh->vertices[j].p.z + mesh->vertices[j].tan.z);
		}
		if (binormalFlag)
		{
			glColor3f(1, 0, 1);
			glVertex3f(mesh->vertices[j].p.x, mesh->vertices[j].p.y, mesh->vertices[j].p.z);
			glVertex3f(mesh->vertices[j].p.x + mesh->vertices[j].bin.x, mesh->vertices[j].p.y + mesh->vertices[j].bin.y, mesh->vertices[j].p.z + mesh->vertices[j].bin.z);
		}
	}

	glEnd();

	glPopAttrib();
}

Mesh *cMesh(size_t nVerts, size_t nIndices)
{
	Mesh *mesh = (Mesh*) malloc(sizeof(Mesh));
	mesh->nVerts = nVerts;
	mesh->nIndices = nIndices;
	mesh->vertices = (Vertex*) calloc(mesh->nVerts, sizeof(Vertex));
	mesh->indices = (int*) calloc(mesh->nIndices, sizeof(int));
	return mesh;
}

void dMesh(Mesh *mesh)
{
	free(mesh->vertices);
	free(mesh->indices);
	free(mesh);
}

void calcTangents(Mesh *mesh)
{
	// You can pretty much ignore this function. It creates tangent and binormal vectors for arbitrary mesh data, which is used by the square/cube/sphere.
	vec3f *tan1 = (vec3f*) calloc(mesh->nVerts * 2, sizeof(vec3f));
	vec3f *tan2 = tan1 + mesh->nVerts;

	for (size_t i = 0; i < mesh->nIndices; i += 3)
	{
		size_t i1 = mesh->indices[i];
		size_t i2 = mesh->indices[i + 1];
		size_t i3 = mesh->indices[i + 2];

		vec3f p1 = mesh->vertices[i1].p;
		vec3f p2 = mesh->vertices[i2].p;
		vec3f p3 = mesh->vertices[i3].p;

		vec2f t1 = mesh->vertices[i1].t;
		vec2f t2 = mesh->vertices[i2].t;
		vec2f t3 = mesh->vertices[i3].t;

		vec3f v = subVec3f(p2, p1);
		vec3f u = subVec3f(p3, p1);

		vec2f s = subVec2f(t2, t1);
		vec2f t = subVec2f(t3, t1);

		float r = 1.0f / (s.x * t.y - t.x * s.y);

		vec3f sdir = cVec3f((t.y * v.x - s.y * u.x) * r, (t.y * v.y - s.y * u.y) * r, (t.y * v.z - s.y * u.z) * r);
		vec3f tdir = cVec3f((s.x * u.x - t.x * v.x) * r, (s.x * u.y - t.x * v.y) * r, (s.x * u.z - t.x * v.z) * r);

		tan1[i1] = addVec3f(tan1[i1], sdir);
		tan1[i2] = addVec3f(tan1[i2], sdir);
		tan1[i3] = addVec3f(tan1[i3], sdir);

		tan2[i1] = addVec3f(tan2[i1], tdir);
		tan2[i2] = addVec3f(tan2[i2], tdir);
		tan2[i3] = addVec3f(tan2[i3], tdir);
	}

	for (size_t i = 0; i < mesh->nVerts; i++)
	{
		//vec3f t = tan1[i];
		//vec3f n = mesh->vertices[i].n;
		//vec3f tan = normaliseVec3f(mulVec3f(subVec3f(t, n), dotVec3f(n, t)));
		//float w = (dotVec3f(cross(n, t), tan2[i]) < 0) ? -1 : 1;

		mesh->vertices[i].tan = normaliseVec3f(tan1[i]);
		mesh->vertices[i].bin = normaliseVec3f(tan2[i]);
	}

	free(tan1);
}

void calcNormals(Mesh *mesh, bool smoothFlag)
{
	// You can pretty much ignore this function. It creates normal vectors from arbitrary mesh data (assuming vertices have correct winding), here for completeness.
	// This function expects vertices to be connected when calculating smooth normals, and disconnected when calculating face normals.
	for (size_t i = 0; i < mesh->nIndices; i += 3)
	{
		int i1 = mesh->indices[i];
		int i2 = mesh->indices[i + 1];
		int i3 = mesh->indices[i + 2];

		vec3f v = subVec3f(mesh->vertices[i2].p, mesh->vertices[i1].p);
		vec3f u = subVec3f(mesh->vertices[i3].p, mesh->vertices[i1].p);
		vec3f n = normaliseVec3f(cross(v, u));

		if (smoothFlag)
		{
			mesh->vertices[i1].n = normaliseVec3f(addVec3f(mesh->vertices[i1].n, n));
			mesh->vertices[i2].n = normaliseVec3f(addVec3f(mesh->vertices[i2].n, n));
			mesh->vertices[i3].n = normaliseVec3f(addVec3f(mesh->vertices[i3].n, n));
		}
		else
		{
			mesh->vertices[i1].n = n;
			mesh->vertices[i2].n = n;
			mesh->vertices[i3].n = n;
		}
	}
}

Mesh *makeCube()
{
	// Apparently creating mesh data for a cube takes more lines than for the terrain :(
	Mesh *mesh = cMesh(24, 36);

	// Front
	mesh->vertices[0].p  = cVec3f( 1, -1,  1); mesh->vertices[0].t  = cVec2f(0, 0);
	mesh->vertices[1].p  = cVec3f( 1,  1,  1); mesh->vertices[1].t  = cVec2f(0, 1);
	mesh->vertices[2].p  = cVec3f(-1,  1,  1); mesh->vertices[2].t  = cVec2f(1, 1);
	mesh->vertices[3].p  = cVec3f(-1, -1,  1); mesh->vertices[3].t  = cVec2f(1, 0);

	// Back
	mesh->vertices[4].p  = cVec3f( 1, -1, -1); mesh->vertices[4].t  = cVec2f(0, 0);
	mesh->vertices[5].p  = cVec3f(-1, -1, -1); mesh->vertices[5].t  = cVec2f(0, 1);
	mesh->vertices[6].p  = cVec3f(-1,  1, -1); mesh->vertices[6].t  = cVec2f(1, 1);
	mesh->vertices[7].p  = cVec3f( 1,  1, -1); mesh->vertices[7].t  = cVec2f(1, 0);

	// Left
	mesh->vertices[8].p  = cVec3f(-1, -1, -1); mesh->vertices[8].t  = cVec2f(0, 0);
	mesh->vertices[9].p  = cVec3f(-1, -1,  1); mesh->vertices[9].t  = cVec2f(0, 1);
	mesh->vertices[10].p = cVec3f(-1,  1,  1); mesh->vertices[10].t = cVec2f(1, 1);
	mesh->vertices[11].p = cVec3f(-1,  1, -1); mesh->vertices[11].t = cVec2f(1, 0);

	// Right
	mesh->vertices[12].p = cVec3f( 1, -1, -1); mesh->vertices[12].t = cVec2f(0, 0);
	mesh->vertices[13].p = cVec3f( 1,  1, -1); mesh->vertices[13].t = cVec2f(0, 1);
	mesh->vertices[14].p = cVec3f( 1,  1,  1); mesh->vertices[14].t = cVec2f(1, 1);
	mesh->vertices[15].p = cVec3f( 1, -1,  1); mesh->vertices[15].t = cVec2f(1, 0);

	// Top
	mesh->vertices[16].p = cVec3f(-1,  1, -1); mesh->vertices[16].t = cVec2f(0, 0);
	mesh->vertices[17].p = cVec3f(-1,  1,  1); mesh->vertices[17].t = cVec2f(0, 1);
	mesh->vertices[18].p = cVec3f( 1,  1,  1); mesh->vertices[18].t = cVec2f(1, 1);
	mesh->vertices[19].p = cVec3f( 1,  1, -1); mesh->vertices[19].t = cVec2f(1, 0);

	// Bottom
	mesh->vertices[20].p = cVec3f(-1, -1, -1); mesh->vertices[20].t = cVec2f(0, 0);
	mesh->vertices[21].p = cVec3f( 1, -1, -1); mesh->vertices[21].t = cVec2f(0, 1);
	mesh->vertices[22].p = cVec3f( 1, -1,  1); mesh->vertices[22].t = cVec2f(1, 1);
	mesh->vertices[23].p = cVec3f(-1, -1,  1); mesh->vertices[23].t = cVec2f(1, 0);

	for (int i = 0; i < 6; i++)
	{
		mesh->indices[i * 6] = i * 4;
		mesh->indices[i * 6 + 1] = i * 4 + 1;
		mesh->indices[i * 6 + 2] = i * 4 + 2;
		mesh->indices[i * 6 + 3] = i * 4;
		mesh->indices[i * 6 + 4] = i * 4 + 2;
		mesh->indices[i * 6 + 5] = i * 4 + 3;
	}

	calcNormals(mesh, false);
	calcTangents(mesh);

	return mesh;
}

Mesh *makeSphere()
{
	// Refer to lecture on procedural modelling if you're unsure what's going on here (http://goanna.cs.rmit.edu.au/~gl/teaching/Interactive3D/2013/lecture6.html)
	// Otherwise feel free to ignore it (this function is a little dodgy as some vertices are repeated, so it will give strange tangent/normal vectors at seams).
	size_t stacks = 11, slices = 11;
	Mesh *mesh = cMesh(stacks * slices, (stacks - 1) * (slices - 1) * 6);

	for (size_t stack = 0; stack < stacks; stack++)
	{
		float phi = stack * M_PI / (float) (stacks - 1);

		for (size_t slice = 0; slice < slices; slice++)
		{
			size_t pos = slice * stacks + stack;
			float theta = slice * 2.0f * M_PI / (float) (slices - 1);
			float x = sinf(phi) * cosf(theta);
			float z = sinf(phi) * sinf(theta);
			float y = cosf(phi);
			float l = lenVec3f(cVec3f(x, y, z));
			mesh->vertices[pos].p = cVec3f(x, y, z);
			mesh->vertices[pos].t = cVec2f(1.0f - (float) slice / (float) slices, 1.0f - (float) stack / (float) stacks);
			mesh->vertices[pos].n = cVec3f(x / l, y / l, z / l);
		}
	}

	size_t i = 0;
	for (size_t stack = 0; stack < stacks - 1; stack++)
	{
		for (size_t slice = 0; slice < slices - 1; slice++)
		{
			mesh->indices[i++] = slice * stacks + stack;
			mesh->indices[i++] = (slice + 1) * stacks + stack + 1;
			mesh->indices[i++] = slice * stacks + stack + 1;
			mesh->indices[i++] = slice * stacks + stack;
			mesh->indices[i++] = (slice + 1) * stacks + stack;
			mesh->indices[i++] = (slice + 1) * stacks + stack + 1;
		}
	}

	calcTangents(mesh);

	return mesh;
}

Mesh *makeSquare()
{
	// This is mostly just here for debugging.
	Mesh *mesh = cMesh(4, 6);

	mesh->vertices[0].p = cVec3f( 1, -1, 0); mesh->vertices[0].t = cVec2f(0, 0); mesh->vertices[0].n = cVec3f(0, 0, 1);
	mesh->vertices[1].p = cVec3f( 1,  1, 0); mesh->vertices[1].t = cVec2f(0, 1); mesh->vertices[1].n = cVec3f(0, 0, 1);
	mesh->vertices[2].p = cVec3f(-1,  1, 0); mesh->vertices[2].t = cVec2f(1, 1); mesh->vertices[2].n = cVec3f(0, 0, 1);
	mesh->vertices[3].p = cVec3f(-1, -1, 0); mesh->vertices[3].t = cVec2f(1, 0); mesh->vertices[3].n = cVec3f(0, 0, 1);

	mesh->indices[0] = 0;
	mesh->indices[1] = 1;
	mesh->indices[2] = 2;
	mesh->indices[3] = 0;
	mesh->indices[4] = 2;
	mesh->indices[5] = 3;

	calcTangents(mesh);
	calcNormals(mesh, false);

	return mesh;
}

void renderMesh(Mesh *mesh, DebugControls *controls)
{

	
	// Note that this function iterates over indices rather than vertices, since the indices provide the order that vertex data needs to appear in.
	glBegin(GL_TRIANGLES);
	


	for (size_t i = 0; i < mesh->nIndices; i++)
	{
		int j = mesh->indices[i];
		glTexCoord2f(mesh->vertices[j].t.x, mesh->vertices[j].t.y);
		glNormal3f(mesh->vertices[j].n.x, mesh->vertices[j].n.y, mesh->vertices[j].n.z);
		glVertex3f(mesh->vertices[j].p.x, mesh->vertices[j].p.y, mesh->vertices[j].p.z);
	}

	glEnd();

	if (controls->axisFlag)
		renderAxes();
	renderLines(mesh, controls->normalFlag, controls->tangentFlag, controls->binormalFlag);
}


/*
void renderMeshNoTexture(Mesh *mesh, DebugControls *controls)
{

	
	// Note that this function iterates over indices rather than vertices, since the indices provide the order that vertex data needs to appear in.
	glBegin(GL_TRIANGLES);
	


	for (size_t i = 0; i < mesh->nIndices; i++)
	{
		int j = mesh->indices[i];
		glNormal3f(mesh->vertices[j].n.x, mesh->vertices[j].n.y, mesh->vertices[j].n.z);
		glVertex3f(mesh->vertices[j].p.x, mesh->vertices[j].p.y, mesh->vertices[j].p.z);
	}

	glEnd();

	if (controls->axisFlag)
		renderAxes();
	renderLines(mesh, controls->normalFlag, controls->tangentFlag, controls->binormalFlag);
}



*/










