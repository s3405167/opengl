#pragma once

#if __cplusplus
extern "C" {
#endif

#include "camera.h"
#include "grid.h"
#include "light.h"
#include "gl.h"
#include "player.h"
void makeTree( vec3f a, Camera *camera, GLuint texture);
void getplacementTree(vec3f *b, Grid *grid);
int comp(const void *a, const void *b);
void sortBillBoard(vec3f *b);
//void renderTrees(Camera *camera,vec3f *a, Grid *grid);
void renderTrees(Camera *camera, vec3f *a, GLuint texture);
#if __cplusplus
}
#endif
