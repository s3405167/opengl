#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"

typedef struct Player Player;
typedef struct PlayerControls PlayerControls;
typedef struct Grid Grid;
typedef struct DebugControls DebugControls;
typedef struct Mesh Mesh;

///
/// Contains all data needed to position and move/rotate an object in a world (in this case the player).
///
struct Player
{
	vec3f pos;
	vec3f rot;
	vec3f vel;
	vec3f forward; //<-is this similiar to acceleration?
	vec3f acceleration;	


	float speed;
	float mass;
	float drag_Cofficient;
	float turnSpeed;
};

///
/// Intialises variables in the player structs using the given arguments. Expects player to have already been constructed.
///
void initPlayer(Player *player, Grid *grid, vec3f pos, vec3f rot, float speed, float turnSpeed, float mass, float cd);

///
/// Updates the position and orientation of the player.
///
void updatePlayer(Player *player, float dt, Grid *grid, PlayerControls *controls);

///
/// Renders the given mesh, using data in the player to position it correctly in the world.
///
void renderPlayer(Player *player, Mesh *mesh, DebugControls *controls);

#if __cplusplus
}
#endif
