

#include "camera.h"
#include "controls.h"
#include "player.h"
#include "particle.h"
#include "gl.h"
#include "stdio.h"


void CreateArrayOfSnowball(Particle *particle, Player *player){
	int i;
	for(i = 0; i < 100; i++){
		#if 0
		
		particle[i].pos.x = randomValueX;
		particle[i].pos.y = randomValueY;
		particle[i].pos.z = randomValueZ;
		//printf("Array Number = %d,snowball pos.x = %f, snowball pos.y = %f, snowball pos.z = %f \n", i, particle[i].pos.x, particle[i].pos.y, particle[i].pos.z );
		#endif
		float randomValueX= rand() % 10 + 1;
		float randomValueY= rand() % 10 + 1;
		float randomValueZ= rand() % 10 + 1;
		particle[i].pos.x = player->pos.x;
		particle[i].pos.y = player->pos.y;
		particle[i].pos.z = player->pos.z;
		particle[i].spawn.x = player->pos.x;
		particle[i].spawn.y = player->pos.y;
		particle[i].spawn.z = player->pos.z;	
		particle[i].directionVector.x = randomValueX;
		particle[i].directionVector.y = randomValueY;
		particle[i].directionVector.z = randomValueZ;

		printf("Array Number = %d,snowball dV.x = %f, snowball dV.y = %f, snowball dV.z = %f \n", i, particle[i].directionVector.x, particle[i].directionVector.y, particle[i].directionVector.z );
		
	}
}

void MakeSnowball( Particle snowball, Camera *camera, GLuint  textureSnowball){
	glPushMatrix();
	glTranslatef(snowball.pos.x,snowball.pos.y,snowball.pos.z); // Move the snowball
	//glTranslatef(0,0,0); // Move the the tree's position
	glRotatef(-(camera->rot.y), 0, 1, 0); // Rotate tree in place
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, textureSnowball);
	glEnable(GL_POINT_SPRITE);
	glEnable(GL_POINT_SMOOTH);
	glTexEnvi(GL_POINT_SPRITE, GL_COORD_REPLACE, GL_TRUE);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE);
	glPointSize(5);
	glBegin(GL_POINTS);
	glVertex3f(0, 0, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();

}


void UpdatingSnowEffect(float dt, Particle *snowball, Player *player, float time){
	/*
	int i;
	if(time <= 5){
		time = time + dt;
		for(i = 0; i < 100; i++){
			snowball[i].pos.x += snowball[i].directionVector.x * dt * 0.1;
			snowball[i].pos.y += snowball[i].directionVector.y * dt * 0.1;
			snowball[i].pos.z += snowball[i].directionVector.z * dt * 0.1;
		
			}
	}else{
		time = 0;
		for(i = 0; i < 100; i++){
			snowball[i].pos.x = player->pos.x;
			snowball[i].pos.y = player->pos.y;
			snowball[i].pos.z = player->pos.z;
			}
	
	}
	*/
			int i;
			for(i = 0; i < 100; i++){
			snowball[i].pos.x += snowball[i].directionVector.x * dt *0.1;
			snowball[i].pos.y += snowball[i].directionVector.y * dt *0.1;
			snowball[i].pos.z += snowball[i].directionVector.z * dt *0.1;	
			}
	
		//printf("Array Number = %d,snowball pos.x = %f, snowball pos.y = %f, snowball pos.z = %f \n", i, snowball[1].pos.x, snowball[1].pos.y, snowball[1].pos.z );
}

