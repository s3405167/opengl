#include "gl.h"
#include "camera.h"
#include "player.h"
#include "skybox.h"

void renderSkyBox(GLuint *texture, Player *player, Camera *camera, float size){

	glPushMatrix();
	glLoadIdentity();
	gluLookAt(0,0,0,1, 1, 1, 0, 1, 0);
	//glTranslatef(player->pos.x, player->pos.y, player->pos.x);
	//glRotatef(-(camera->rot.y), 0, 1, 0);
	
	glPushAttrib(GL_ENABLE_BIT);
	glEnable(GL_TEXTURE_2D);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);
	glDisable(GL_BLEND);
	
	glColor4f(1,1,1,1);

	//Render the front quad

	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glBegin(GL_QUADS);
		glTexCoord2f(0,0); glVertex3f(size, -size, -size);
		glTexCoord2f(1,0); glVertex3f(-size, -size, -size);
		glTexCoord2f(1,0); glVertex3f(-size, size, -size);
		glTexCoord2f(0,1); glVertex3f(size, size, -size);
	glEnd();
	

//Render the left quad
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glBegin(GL_QUADS);
		glTexCoord2f(0,0); glVertex3f(size, -size, size);
		glTexCoord2f(1,0); glVertex3f(size, -size, -size);
		glTexCoord2f(1,0); glVertex3f(size, size, -size);
		glTexCoord2f(0,1); glVertex3f(size, size, size);
	glEnd();


	glBindTexture(GL_TEXTURE_2D, texture[2]);
	glBegin(GL_QUADS);
		glTexCoord2f(0,0); glVertex3f(-size, -size, size);
		glTexCoord2f(1,0); glVertex3f(size, -size, size);
		glTexCoord2f(1,0); glVertex3f(size, size, size);
		glTexCoord2f(0,1); glVertex3f(-size, size, size);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, texture[3]);
	glBegin(GL_QUADS);
		glTexCoord2f(0,0); glVertex3f(-size, -size, -size);
		glTexCoord2f(1,0); glVertex3f(-size, -size, size);
		glTexCoord2f(1,0); glVertex3f(-size,size, size);
		glTexCoord2f(0,1); glVertex3f(-size, size, -size);
	glEnd();

	glBindTexture(GL_TEXTURE_2D, texture[4]);
	glBegin(GL_QUADS);
		glTexCoord2f(0,0); glVertex3f(-size, size, -size);
		glTexCoord2f(1,0); glVertex3f(-size, size, size);
		glTexCoord2f(1,0); glVertex3f(size, size, size);
		glTexCoord2f(0,1); glVertex3f(size, size, -size);
	glEnd();

		glBindTexture(GL_TEXTURE_2D, texture[5]);
	glBegin(GL_QUADS);
		glTexCoord2f(0,0); glVertex3f(-size, -size, -size);
		glTexCoord2f(1,0); glVertex3f(-size, -size, size);
		glTexCoord2f(1,0); glVertex3f(size, -size, size);
		glTexCoord2f(0,1); glVertex3f(size, -size, -size);
	glEnd();
	
	glPopAttrib();
	glPopMatrix();

	
}
