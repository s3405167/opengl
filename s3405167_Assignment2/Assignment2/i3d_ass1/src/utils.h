///
/// @file   utils.h
///
/// I'm so not documenting all the functions/structs in this file.
///
///
/// File contains structs for vectors with 2, 3 and 4 components, as well as functions for some basic vector operations for each, which includes:
///
///    - construction
///    - addition
///    - subtraction
///    - multiplication
///    - dot product
///    - length squared (allows faster distance comparisons than length)
///    - length
///    - normalise
///
///
/// Unfortunately C has no operator overloading, so these operations are put into *really ugly* functions. If you plan to use the code beyond assignment 2 and you want
/// to stay sane, I recommend porting this to a language with operator overloading (like C++).
///

#pragma once

#if __cplusplus
extern "C" {
#endif

#if _WIN32
#define bool char
#define true 1
#define false 0
#else
#include <stdbool.h>
#endif

#include <stdlib.h>
#include <stdarg.h>

#ifndef _WIN32
#define min(a, b) (((a) < (b)) ? (a) : (b))
#define max(a, b) (((a) > (b)) ? (a) : (b))
#endif

#define clamp(x, a, b) min(max(x, a), b)

#ifndef M_PI
#define M_PI 3.141592653589793238462643
#endif

typedef struct vec2f vec2f;
typedef struct vec3f vec3f;
typedef struct vec4f vec4f;
typedef struct SineFunction SineFunction;

struct vec2f
{
	float x, y;
};

vec2f cVec2f(float x, float y);
vec2f addVec2f(vec2f v1, vec2f v2);
vec2f subVec2f(vec2f v1, vec2f v2);
vec2f mulVec2f(vec2f v1, float f);
float dotVec2f(vec2f v1, vec2f v2);
float lenSqVec2f(vec2f v);
float lenVec2f(vec2f v);
vec2f normaliseVec2f(vec2f v);


struct vec3f
{
	float x, y, z;
};

vec3f cVec3f(float x, float y, float z);
vec3f addVec3f(vec3f v1, vec3f v2);
vec3f subVec3f(vec3f v1, vec3f v2);
vec3f mulVec3f(vec3f v1, float f);
float dotVec3f(vec3f v1, vec3f v2);
float lenSqVec3f(vec3f v);
float lenVec3f(vec3f v);
vec3f normaliseVec3f(vec3f v);


struct vec4f
{
	float x, y, z, w;
};


vec4f cVec4f(float x, float y, float z, float w);
vec4f addVec4f(vec4f v1, vec4f v2);
vec4f subVec4f(vec4f v1, vec4f v2);
vec4f mulVec4f(vec4f v1, float f);
float dotVec4f(vec4f v1, vec4f v2);
float lenSqVec4f(vec4f v);
float lenVec4f(vec4f v);
vec4f normaliseVec4f(vec4f v);

vec3f cross(vec3f v, vec3f u);


///
/// A single sine function that can vary on both the x and z axes (or just one). Phase is only useful if you want to animate it.
///
struct SineFunction
{
	float amplitude;
	float phase;
	float periodX;
	float periodZ;
};

///
/// Constructs the sine wave, returning a struct populated with the given values.
///
SineFunction cSineFunction(float amplitude, float phase, float periodX, float periodZ);

///
/// Multiplies the x and z values by their respective periods, and adds the phase. Here to avoid unnecessary code repetition.
///
float calcAngle(SineFunction *f, float x, float z);

///
/// Returns the height (y value) of the sine function at the given x,z position.
///
float calcHeight(SineFunction *f, float x, float z);

///
/// Returns the normal vector of the sine function at the given x,z position.
///
vec3f calcNormal(SineFunction *f, float x, float z);

///
/// Returns the tangent vector of the sine function at the given x,z position.
///
vec3f calcTangent(SineFunction *f, float x, float z);

///
/// Returns the binormal vector of the sine function at the given x,z position.
///
vec3f calcBinormal(SineFunction *f, float x, float z);



#if __cplusplus
}
#endif
