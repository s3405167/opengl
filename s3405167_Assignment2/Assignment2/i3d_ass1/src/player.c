#include "player.h"
#include "controls.h"
#include "grid.h"
#include "mesh.h"
#include "gl.h"
#include "stdio.h"
#include <math.h>

#define UNUSED(x) (void)(x)

const float g = -9.8;

static void updatePlayerPos(Player *player, PlayerControls *controls, Grid *grid, float dt)
{
	// dir determines wether the player is moving forwards or backwards (or not at all).
	float fSum, weight, fDrag;
	float dir = 1;

	weight = player->mass * g;

	fDrag = player->drag_Cofficient * (player->vel.x * player->vel.x + player->vel.y * player->vel.y + player->vel.z * player->vel.z);
	//printf("Fdrag = %lf \n", fDrag);
	


	
	fSum = weight * sinf(player->rot.x);//+fDrag;	
	//printf("fSum = %lf \n", fSum);
	//Acceleration
	//printf("player->acceleration.y = %lf \n", player->acceleration.y);
	//player->acceleration.x = fSum * player->forward.x / player->mass;
	
	//player->acceleration.z = fSum * player->forward.z / player->mass;
	
	player->acceleration.y =  (player->forward.x) /weight;
	player->acceleration.x =   (player->forward.x);
	player->acceleration.z =   (player->forward.z);
	

//Debug
#if 0
	printf("player->rot.x = %lf \n", player->rot.x);
	printf("player->rot.y = %lf \n", player->rot.y);
	printf("player->rot.z = %lf \n", player->rot.z);

	printf("player->acceleration.x = %lf \n", player->acceleration.x);
	printf("player->acceleration.y = %lf \n", player->acceleration.y);
	printf("player->acceleration.z = %lf \n", player->acceleration.z);
#endif	
	//float 
	if (controls->up && player->speed < 5.0)
		player->speed += 0.05;
	if (controls->down && player->speed >= 0)
		player->speed -= 0.05;
		
		//dir -= 1;
	if(player->speed <= 0.0)
		player->speed = 0.0;
	// turn determines the rotation of the player around its y axis.

	
	float turn = 0;
	//printf("speed = %lf \n", player->speed);
	if (controls->left)
		turn += player->turnSpeed;
	if (controls->right)
		turn -= player->turnSpeed;
	player->rot.y += turn * dt;

	// y position of the player.
	float gridHeight = accumHeight(grid, player->pos.x, player->pos.z) + 1;
	
	// velocity is calculated using the x and z values of the player's forward vector (which is calculated using the player's y rotation).

	player->vel.x = player->forward.x * player->speed * dir;
	player->vel.z = player->forward.z * player->speed * dir;
	// position is simple to calculate when given correct velocity (note that velocity does not need to be the same as the forward direction of the player).
	//if(player->pos.y <= gridHeight){
	

//Testing slope acceralation
	#if 0
	player->vel.x += player->acceleration.x * dt * weight;
	player->vel.z += player->acceleration.z * dt * weight;
	printf("player->vel.x = %lf \n",player->vel.x );
	printf("player->vel.z = %lf \n",player->vel.z );
	player->vel.y = player->acceleration.y;
	player->vel.x += player->acceleration.x * dir * player->speed;
	player->vel.z += player->acceleration.z * dir * player->speed;
	#endif
	//}
	//player->pos.y += player->vel.y;

	//if(player->pos.y <= gridHeight)
	player->pos.y = gridHeight;

	player->pos.x += dt * (player->vel.x + dt * player->acceleration.x *0.5);
	player->pos.z += dt * (player->vel.z + dt * player->acceleration.z *0.5);
	
}

static void orientPlayer(Player *player, Grid *grid, float dt)
{
	UNUSED(dt);

	// only x and z values of the player's forward vector are needed right now, but y is provided as well for completeness (it'll be useful later).
	//is this using Polar coordinate system ?	
	
	player->forward.x = sinf(player->rot.y * (float) M_PI / 180.0f);
	player->forward.z = cosf(player->rot.y * (float) M_PI / 180.0f);
	player->forward.y = -sinf(player->rot.x * (float) M_PI / 180.0f);

	vec3f v = accumNormal(grid, player->pos.x, player->pos.z);

	// the player's rotation around the x (pitch) axis is calculated with a dot product of the normal vector, with the forward vector (y value is not needed).
	player->rot.x = dotVec3f(cVec3f(player->forward.x, 0, player->forward.z), v) * 180.0f / (float) M_PI;

	// the player's right vector can be calculated in the same way as the forward vector, and a dot product used to give the rotation on z (roll).
	float rx = sinf((player->rot.y - 90.0f) * (float) M_PI / 180.0f);
	float rz = cosf((player->rot.y - 90.0f) * (float) M_PI / 180.0f);
	player->rot.z = dotVec3f(cVec3f(rx, 0, rz), v) * 180.0f / (float) M_PI;

	// if you want to be tricky, you don't actually need any trig for the z rotation, ie:
	// float rx = sinf((player->rot.y - 90.0f) * (float) M_PI / 180.0f);
	// float rz = cosf((player->rot.y - 90.0f) * (float) M_PI / 180.0f);
	//
	// is the same as:
	// float rx = -cosf(player->rot.y * (float) M_PI / 180.0f);
	// float rz = sinf(player->rot.y * (float) M_PI / 180.0f);
	//
	// which is the same as:
	// float rx = -player->forward.z;
	// float rz = player->forward.x;
	//
	// therefore the final calculation for z (roll) is:
	// player->rot.z = dotVec3f(cVec3f(-player->forward.z, 0, player->forward.x), v) * 180.0f / (float) M_PI;
}




void initPlayer(Player *player, Grid *grid, vec3f pos, vec3f rot, float speed, float turnSpeed, float mass, float cd)
{
	player->forward = cVec3f(0, 0, 1);
	player->pos = pos;
	player->rot = rot;
	player->speed = speed;
	player->turnSpeed = turnSpeed;
	player->mass = mass;
	player->drag_Cofficient = cd;
	player->pos.y = accumHeight(grid, player->pos.x, player->pos.z) + 1;
}

void updatePlayer(Player *player, float dt, Grid *grid, PlayerControls *controls)
{
	updatePlayerPos(player, controls, grid, dt);
	orientPlayer(player, grid, dt);
}

void renderPlayer(Player *player, Mesh *mesh, DebugControls *controls)
{
	static float diffuse[] = { 1, 0.5f, 0, 1 };
	static float ambient[] = { 0.4f, 0.2f, 0, 1 };
	static float specular[] = { 1, 0.5f, 0, 1 };
	static float shininess = 32.0f;

	glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, ambient);
	glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, diffuse);
	glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, specular);
	glMaterialf(GL_FRONT_AND_BACK, GL_SHININESS, shininess);
	//Complex Model
	glPushMatrix();

	glTranslatef(player->pos.x, player->pos.y+1, player->pos.z);
	glRotatef(player->rot.y, 0, 1, 0);
	glRotatef(player->rot.x, 1, 0, 0);
	glRotatef(player->rot.z, 0, 0, 1);
	//glScalef(0.5,0.75,0.5);
	renderMesh(mesh, controls);
	
		//Render head 
		glPushMatrix();
		glTranslatef(0,1,0.0);
		glScalef(0.75,0.75,0.75);
		renderMesh(mesh, controls);
	
		glPopMatrix();

		//Render left upper arm 
		glPushMatrix();
		glTranslatef(1,0.3,0.3);
		glRotatef(90, 1, 0, 0);

		glScalef(0.25,0.5,0.25);
		renderMesh(mesh, controls);

		//Render left lower arm 
			glPushMatrix();
			glTranslatef(1,0.3,0.3);
			//glScalef(0.25,0.5,0.25);
			renderMesh(mesh, controls);
			glPopMatrix();
		glPopMatrix();
	

	

		//Render right upper leg 
		glPushMatrix();
		glTranslatef(-1,0,0.3);
		glRotatef(90, 1, 0, 0);

		glScalef(0.25,0.5,0.25);
		renderMesh(mesh, controls);

		//Render right lower leg 
			glPushMatrix();
			glTranslatef(-1,0,0.3);
			//glScalef(0.25,0.5,0.25);
			renderMesh(mesh, controls);
			glPopMatrix();
		glPopMatrix();



		//Render left upper leg 
		glPushMatrix();
		glTranslatef(1,-1,0.3);
		glRotatef(90, 1, 0, 0);

		glScalef(0.25,0.5,0.25);
		renderMesh(mesh, controls);

		//Render left lower leg 
			glPushMatrix();
			glTranslatef(1,0.3,0.3);
			//glScalef(0.25,0.5,0.25);
			renderMesh(mesh, controls);
			glPopMatrix();
		glPopMatrix();
	

	

		//Render right upper leg 
		glPushMatrix();
		glTranslatef(-1,-1,0.3);
		glRotatef(90, 1, 0, 0);

		glScalef(0.25,0.5,0.25);
		renderMesh(mesh, controls);

		//Render right lower leg 
			glPushMatrix();
			glTranslatef(-1,-1,0.3);
			//glScalef(0.25,0.5,0.25);
			renderMesh(mesh, controls);
			glPopMatrix();
		glPopMatrix();	


	//Render 
	
	glPopMatrix();
}
