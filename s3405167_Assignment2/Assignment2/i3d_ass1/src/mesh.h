#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"

typedef struct Vertex Vertex;
typedef struct Mesh Mesh;
typedef struct DebugControls DebugControls;

///
/// Not all vertices need all this information, but this keeps things simple. Vertex contains position, normal, texture coordinate, tangent, and binormal data.
///
struct Vertex
{
	vec3f p;
	vec3f n;
	vec2f t;
	vec3f tan;
	vec3f bin;
};

///
/// Mesh contains an array of unique vertices, and an array of indices that index the vertex array to prevent unnecessary repetition of data.
///
struct Mesh
{
	size_t nVerts;
	Vertex *vertices;

	size_t nIndices;
	int *indices;
};

///
/// Allocates memory for a triangle mesh of a given size.
///
Mesh *cMesh(size_t nVerts, size_t nIndices);

///
/// Frees memory associated with mesh.
///
void dMesh(Mesh *mesh);

///
/// Calculates tangents and binormals for arbitrary mesh data. Expects mesh vertices and indices to already have been created, with correct vertex winding.
///
void calcTangents(Mesh *mesh);

///
/// Calculates smooth or face normals for arbitrary mesh data. Expects mesh vertices and indices to already have been create, with correct vertex winding.
///
void calcNormals(Mesh *mesh, bool smoothFlag);

///
/// Creates a cube with min of (-1, -1, -1) and max of (1, 1, 1).
///
Mesh *makeCube();

///
/// Creates a sphere with radius 1.
///
Mesh *makeSphere();

///
/// Creates a square with min (-1, -1, 0) and max of (1, 1, 0).
///
Mesh *makeSquare();

///
/// Renders given mesh in immediate mode. Will render lines for axes/normals/tangents/binormals depending on values given in debug controls.
///
void renderMesh(Mesh *mesh, DebugControls *controls);


///
/// Renders given mesh without Texture in immediate mode. Will render lines for axes/normals/tangents/binormals depending on values given in debug controls.
///


//void renderMeshNoTexture(Mesh *mesh, DebugControls *controls)



#if __cplusplus
}
#endif
