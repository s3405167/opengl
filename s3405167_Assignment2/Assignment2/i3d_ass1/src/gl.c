#include "gl.h"
#include <stdlib.h>
#include <stdio.h>
// Program uses the Simple OpenGL Image Library for loading textures: http://www.lonesock.net/soil.html
#include <SOIL.h>




GLuint loadTexture(const char *filename)
{
	GLuint tex = SOIL_load_OGL_texture(filename, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);
	if (!tex){
		printf("Unable to load texture\n");
		return 0;
	}
	
	// You can change the texture coords on the grid if you like, but they're the same ones used for assignment 2 solution, so they are correct
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // You can change these to set how the texture is mapped to the grid
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	return tex;
}

/*

GLuint loadSkyBoxTexture(const char *filename2)
{
	GLuint tex = SOIL_load_OGL_texture(filename2, SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y);
	if (!tex){
		printf("Unable to load texture\n");
		return 0;
	}
	
	// You can change the texture coords on the grid if you like, but they're the same ones used for assignment 2 solution, so they are correct
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP); // You can change these to set how the texture is mapped to the grid
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glBindTexture(GL_TEXTURE_2D, 0);
	
	return tex;
}
*/

