#pragma once

#if __cplusplus
extern "C" {
#endif

#include "camera.h"
#include "mesh.h"
#include "controls.h"
#include "player.h"
#include "gl.h"

//typedef struct Snowball Snowball;
typedef struct Particle Particle;

struct Particle{
	vec3f pos;
	vec3f spawn;
	vec3f directionVector;
};

void CreateArrayOfSnowball(Particle *particle, Player *player);
void MakeSnowball( Particle snowball, Camera *camera, GLuint  textureSnowball);
void UpdatingSnowEffect(float dt, Particle *snowball, Player *player, float time);

#if __cplusplus
}
#endif
