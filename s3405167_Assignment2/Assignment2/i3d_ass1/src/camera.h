#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"

typedef struct Camera Camera;
typedef struct Player Player;

///
/// Very basic camera which can be customised to encompass all or part of the window (using the left/right/height variables).
///
struct Camera
{
	int left;
	int right;
	int height;
	float fov;
	float nearPlane;
	float farPlane;
	float zoom;
	float sensitivity;
	vec3f pos;
	vec2f rot;
	bool perspectiveFlag;
	struct Player *player;
};

///
/// Initialises the camera using left/right, plus a whole lot of default values. Expects camera to have already been constructed.
///
void initCamera(Camera *camera, int left, int right);

///
/// Applies camera transforms to position it correctly in the world (or more accurately, move the world to the correct position). This is the view part of the modelview.
///
void uploadView(Camera *camera);

///
/// Applies projection transforms to have the world appear in either a perspective, or orthographic projection.
///
void uploadProjection(Camera *camera);

///
/// Applies camera rotation using relative x and y mouse movement.
///
void updateRotation(Camera *camera, int dx, int dy);

///
/// Applies camera zooming using relative y mouse movement.
///
void updateZoom(Camera *camera, int dy);

///
/// Sets the player variable of the camera, so it will follow that player as it moves.
///
void attachCameraToPlayer(Camera *camera, Player *player);

///
/// Returns the position that the camera is viewing the world from.
///
vec3f getViewPos(Camera *camera);

#if __cplusplus
}
#endif
