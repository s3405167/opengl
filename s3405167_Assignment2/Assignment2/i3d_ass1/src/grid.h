#pragma once

#if __cplusplus
extern "C" {
#endif

#include "utils.h"

typedef struct Grid Grid;
typedef struct DebugControls DebugControls;

///
/// Contains mesh data for a grid (along the x/z plane), with y values for each vertex displaced by an array of sine functions.
///
struct Grid
{
	size_t rows;
	size_t cols;
	vec2f size;
	float slopeAmt;

	struct Mesh *mesh;

	SineFunction *sineFunctions;
	size_t nSineFunctions;
};

///
/// Creates and populates mesh data for the grid, with the given size and rows/cols representing grid cells. Expects grid to have already been constructed.
///
void initGrid(Grid *grid, vec2f size, size_t rows, size_t cols, float slopeAmt);

///
/// Renders the grid using the renderMesh function, but provides appropriate material data for lighting first.
///
void renderGrid(Grid *grid, DebugControls *controls);

///
/// Calculates y value (height) of the grid for the given x,z point.
///
float accumHeight(Grid *grid, float x, float z);

///
/// Calculates the normal vector of the grid for the given x,z point.
///
vec3f accumNormal(Grid *grid, float x, float z);

///
/// Calculates the tangent vector of the grid for the given x,z point.
///
vec3f accumTangent(Grid *grid, float x, float z);

///
/// Calculates the binormal vector of the grid for the given x,z point.
///
vec3f accumBinormal(Grid *grid, float x, float z);

#if __cplusplus
}
#endif
