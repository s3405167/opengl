#include "globals.h"
#include "gl.h"

static void initRendering()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	glEnable(GL_DEPTH_TEST);
	 //glEnable (GL_BLEND);
	 //glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	 //glShadeModel (GL_FLAT);
	 glShadeModel(GL_SMOOTH);

	glClearColor(0, 0, 0, 0);
}



void initGlobals(Globals *globals)
{
	// glew is for OpenGL2+ functions, in case you want to use them.
	glewInit();

	initPlayerControls(&globals->playerControls);

	initCameraControls(&globals->cameraControls);

	initDebugControls(&globals->debugControls);
	
	initGrid(&globals->grid, cVec2f(200, 800), 100, 800, 0.25f);
	//initPlayer(Player *player, Grid *grid, vec3f pos, vec3f rot, float speed, float turnSpeed, float mass, float cd)
	initPlayer(&globals->player, &globals->grid, cVec3f(-88, 0, 45), cVec3f(0, 10, 0), 0, 45, 0.1, 1);


	initLight(&globals->light, cVec4f(100, 100, -100, 0), cVec4f(0.2f, 0.2f, 0.2f, 1), cVec4f(1, 1, 1, 1), cVec4f(0.1f, 0.1f, 0.1f, 1), 64);

	globals->testCube = makeCube();

	initRendering(globals);

	attachCameraToPlayer(&globals->camera, &globals->player);
}
