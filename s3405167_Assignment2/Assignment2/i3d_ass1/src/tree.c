#include "stdio.h"
#include "tree.h"
#include "camera.h"
#include "grid.h"
#include "light.h"
#include "player.h"
#include "gl.h"


//Render Tree

void makeTree(vec3f a, Camera *camera, GLuint texture)
{
	
	
	glPushMatrix();
	glTranslatef(a.x,a.y,a.z); // Move the the tree's position
	glRotatef(-(camera->rot.y), 0, 1, 0); // Rotate tree in place
	//glTranslatef(globals.player.pos.x - a.x, globals.player.pos.y - a.y, globals.player.pos.z -a.z);

	//glColor4i(1,1,1,1);
	//glEnable(GL_ALPHA_TEST);
	//glAlphaFunc(GL_ALWAYS, 0);
	
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	//glDepthMask(GL_FALSE);
	//glDisable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);
	
	//glAlphaFunc(GL_ALWAYS, 0);
	
	glBegin(GL_QUADS);
	glTexCoord2f(0,0);
	glVertex3f(-5, 0, 0);
	glTexCoord2f(1,0);
	glVertex3f(5, 0, 0);
	glTexCoord2f(1,1);
	glVertex3f(5, 5, 0);
	glTexCoord2f(0,1);
	glVertex3f(-5, 5, 0);
	
	#if 0
	glTexCoord2f(0,0);
	glVertex3f(0, 0, -5);
	glTexCoord2f(1,0);
	glVertex3f(0, 0, 5);
	glTexCoord2f(1,1);
	glVertex3f(0, 5, 5);
	glTexCoord2f(0,1);
	glVertex3f(0, 5, -5);
#endif
	glEnd();

	
	glPopMatrix();
	//glTranslatef(globals.player.pos.x - a.x, globals.player.pos.y - a.y, globals.player.pos.z -a.z);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_BLEND);
}


//Generator random 100 tree's position
void getplacementTree(vec3f *b, Grid *grid)
{
	
	//
	#if 0
	if(c < 100){
		for (i = 0; i < 100; ++i){
			float randomValueX= rand() % 200 + 1;
			float randomValueZ= rand() % 800 + 1;
			b[i].x = randomValueX;
			b[i].z = randomValueZ;
			b[i].y = accumHeight(grid, b[i].x, b[i].z);
			printf(" i = %d, x = %f, y = %f, z = %f \n", i, b[i].x, b[i].y, b[i].z);
			c++;
		}
	}
	#endif
	int i;
	
		for (i = 0; i < 100; ++i){
			float randomValueX= rand() % 200 + 1; 
			float randomValueZ= rand() % 800 + 1;
			b[i].x = 100 - randomValueX;
			b[i].z = randomValueZ;
			b[i].y = accumHeight(grid, b[i].x, b[i].z);
			//printf(" i = %d, x = %f, y = %f, z = %f \n", i, b[i].x, b[i].y, b[i].z);
			
		}

}
int comp(const void *a, const void *b){
	
	const struct vec3f *elem1 = a;
	const struct vec3f *elem2 = b;
	if(elem1->z < elem2->z) return -1;
	else if( elem1->z == elem2->z) return 0;
	else return 1;

	
}

//Sort BillBoard 
void sortBillBoard(vec3f *b){
	qsort(b, 100, sizeof(vec3f), comp);
	#if 0
	int n;
	for(n = 0; n < 100; n++)
	printf("Array = %d, tree.pos.z =%lf \n",n, b[n].z );
	#endif
}

//Render all trees
void renderTrees(Camera *camera, vec3f *a, GLuint texture){
	
	for(int i = 0; i < 100; i++)
	{	
		//glPushMatrix();
		makeTree(a[i], camera, texture);
		//glPopMatrix();
		//printf(" i = %d, x = %f, y = %f, z = %f \n", i, a[i].x, a[i].y, a[i].z);
		//printf(" i = %d, x = %lf, y = %lf, z = %lf \n", i, globals.grid.mesh->vertices[i].p.x, globals.grid.mesh->vertices[i].p.y, globals.grid.mesh->vertices[i].p.z);
	}

	

}


